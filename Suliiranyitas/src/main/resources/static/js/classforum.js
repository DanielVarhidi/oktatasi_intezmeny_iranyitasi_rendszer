/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {

    var selectedClassId;

    $.get('/rest/forum/messages', function (messages) {
        $('#classforum').empty();
        for (var i = 0; i < messages.length; i++) {
            var message = messages[i];
            var $tr = createTrFromMessages(message);
            $('#classforum').append($tr);
        }
    });

    function classLinkClicked(link) {
        $('.classLink').removeClass('activeLink');
        $(link).addClass('activeLink');
        selectedClassId = $(link).attr("stclassid");

        $.get('/rest/forum/classmessages/' + $(link).attr("stclassid"), function (messages) {
            $('#classforum').empty();
            for (var i = 0; i < messages.length; i++) {
                var message = messages[i];
                var $tr = createTrFromMessages(message);
                $('#classforum').append($tr);
            }
        });
    }




    $.get('/rest/forum/selectclass', function (classes) {
        $('#classesofteacher').empty();
        var $ul = $("<ul></ul>");
        $('#classesofteacher').append($ul);

        for (var i = 0; i < classes.length; i++) {
            var $li = $("<li></li>");
            var className = classes[i].name;
            var startYear = classes[i].startYear;
            var linkText = className + " " + startYear;
            var linkValue = classes[i].id;
            var $link = $('<a ' + 'href= "#"></a>');
            $link.attr('stClassId', linkValue);
            $link.attr('class', 'classLink');
            $link.click(function (event) {
                event.preventDefault();
                classLinkClicked(this);
            });
            $link.text(linkText);
            $li.append($link);
            $ul.append($li);
        }
    })

    $("#forum-send").on('click', function (e) {
        e.preventDefault();
        var message = {};
        message.author = "";
        message.text = $('#newmessage').val();
        message.studentClassId = selectedClassId;
        var token = $('input[name="_csrf"]').val();
        $.ajax({
            url: "/rest/forum/newmessage",
            contentType: 'application/json',
            method: 'POST',
            data: JSON.stringify(message),
            headers: {
                'X-CSRF-TOKEN': token,
            },
            success: function (message) {
                $tr = createTrFromMessages(message);
                $('#classforum').append($tr);
                message.text = $('#newmessage').val('');
            }
        });

    });

});



function createTrFromMessages(message) {
    $author = $('<td></td>').text(message.author);
    $text = $('<td></td>').text(message.text);
    $tr = $('<tr></tr>').append($author).append($text);
    return $tr;
}