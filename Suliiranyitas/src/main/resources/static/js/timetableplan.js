$(function(){

  var lessons = [];
  var plan;
  var isSelected = false;
  var selectedLesson;
  var $selectedLesson;

  function Lesson(lessonName, lessonId, className, classId, teacherName, teacherId, day, hour){
    this.teacherName = teacherName;
    this.teacherId = teacherId;
    this.className = className;
    this.classId = classId;
    this.lessonName = lessonName;
    this.lessonId = lessonId;
    this.day = day;
    this.hour = hour;
  };

  function updateLesson(lessonItem){
    lessons.forEach(function(elem){
      if(elem.lessonId == lessonItem.id){
        elem.day = lessonItem.day;
        elem.hour = lessonItem.hour;
      }
    })
  }

  function updateAll(data){
    plan = data;
    data.forEach(function(lessonItem){
      updateLesson(lessonItem);
    })
  }

  for (var i = 8; i < 19; i++) {
    $day1 = $('<td></td>').attr('day',"MONDAY").attr('hour',i);
    $day2 = $('<td></td>').attr('day',"TUESDAY").attr('hour',i);
    $day3 = $('<td></td>').attr('day',"WEDNESDAY").attr('hour',i);
    $day4 = $('<td></td>').attr('day',"THURSDAY").attr('hour',i);
    $day5 = $('<td></td>').attr('day',"FRIDAY").attr('hour',i);
    $row = $('<tr></tr>').attr("row",i)
              .append('<th>'+i+'</th>')
              .append($day1)
              .append($day2)
              .append($day3)
              .append($day4)
              .append($day5)
    $('#timetablebody-example').append($row);
  }

  function dayToNumber(day){
    switch (day) {
      case "MONDAY":
        return 1;
        break;
      case "TUESDAY":
        return 2;
        break;
      case "WEDNESDAY":
        return 3;
        break;
      case "THURSDAY":
        return 4;
        break;
      case "FRIDAY":
        return 5;
        break;
      default:
        return 0;
        break;
    }
  }

  function getLessonItems( func ){
    $.get(
        '/rest/timetable/inst',
        function(data){
          data.forEach(function(item){
            console.log(item);
          });

        },
        'json'
      );
  }

  function stringId(str){
    return str.replace(/[\s.]/g,'');
  }

  function lessonToTable(tableId, lessonId, hour, day, text){
    var col = dayToNumber(day);
    var row = hour;
    $p = $('<p></p>').text(text).addClass('lessonItem')
      .attr('day', day).attr('hour', hour).attr('id', lessonId);
    var sel = '#table' + tableId;
    $(sel).find('tr[row='+row+']')
        .children().eq(col)
        .append($p);
  }

  function createTable(id, name){
    $h = $('<h3></h3>').text(name);
    $('#timetables').append($h);
    $('#timetable-example').clone().attr("id", 'table'+id).appendTo('#timetables').removeClass('d-none');
  }

  function clearTable(){
    $('#timetables').children().remove();
  }

  //---------------- load data ------------------
  $.get(
      '/rest/timetable/inst',
      function(data){
        data.forEach(function(item){
          var lesson = new Lesson(
            item.name,
            item.id,
            item.studentClassName,
            item.studentClassId,
            item.teacherName,
            item.teacherId,
            item.day,
            item.hour,
          );
          lessons.push(lesson);
        });
        $('#buttons').removeClass('d-none');
      },
      'json'
    );
    //---------------- --------- ------------------

  $('#byClass').click(function(){
    clearTable();
    lessons.forEach(function(item){
      var tableId = item.classId;
      var select = '#table' + tableId;
      if ( !$(select).length ) {
        createTable(tableId, item.className);
      }
      lessonToTable(tableId, item.lessonId, item.hour, item.day, item.lessonName);
    });
  });

  $('#byTeacher').click(function(){
    clearTable();
    lessons.forEach(function(item){
      var tableId = item.teacherId;
      var select = '#table' + tableId;
      if ( !$(select).length ) {
        createTable(tableId, item.teacherName);
      }
      lessonToTable(tableId, item.lessonId, item.hour, item.day, item.lessonName);
    });
  });

  function checkLessons(){
    var classCollision=0;
    var teacherCollision=0;
    lessons.forEach(function(elem){
      lessons.forEach(function(other){
        if (elem.classId == other.classId && elem.hour == other.hour && elem.day == other.day) {
          classCollision++;
        }
        if (elem.teacherId == other.teacherId && elem.hour == other.hour && elem.day == other.day) {
          teacherCollision++;
        }
      });
    });
    classCollision = (classCollision - lessons.length )/2;
    teacherCollision = (teacherCollision - lessons.length)/2;
    return {'classCollision' : classCollision, 'teacherCollision' : teacherCollision}
  }

  $('#check').click(function(){
    var r = checkLessons();
    var text="ütközés az osztályoknál: " + r.classCollision;
    text += ", ütközés a tanároknál: " + r.teacherCollision;
    $('#plan-result').text(text);
  });

  $('#plan').click(function(){
    $('#plan-result').text('... tervezés folyamatban ...');
    clearTable();
    /*$.get('/rest/timetable/plan',
      function(data){
        console.log(data);
      }
    ,'json');
    */
    $.ajax({
      url : '/rest/timetable/plan',
      method : 'GET',
      dataType : 'json',
      success : function(data){
        $('#plan-result').text('tervezés vége');
        updateAll(data);
      },
      error : function(jqxhr, status, thrown){
        $('#plan-result').text('tervezés sikertelen');
        console.log(status);
      },
    });
  });

  $('body').on('click', '.lessonItem', function(event){
    if (isSelected) {
      $selectedLesson.toggleClass('selected');
    }
    if (isSelected && $selectedLesson.attr('id') == $(this).attr('id')) {
      isSelected = false;
    }else{
      isSelected = true;
      $(this).toggleClass('selected');
      selectedLesson = {id : $(this).attr('id'),
        hour : $(this).attr('hour'),
        day : $(this).attr('day'),
        tableId : $(this).closest('table').attr('id')};
      $selectedLesson = $(this);
    }
    event.stopPropagation();
  });

  $('body').on('click', 'td', function(){
    if (isSelected && selectedLesson.tableId == $(this).closest('table').attr('id')) {
      isSelected = false;
      $(this).append($selectedLesson);
      $selectedLesson.toggleClass('selected');
      selectedLesson.hour = $(this).attr('hour');
      selectedLesson.day = $(this).attr('day');
      $selectedLesson.attr('hour',$(this).attr('hour'));
      $selectedLesson.attr('day',$(this).attr('day'));
      updateLesson(selectedLesson);
    }
  });

  $('#save').click(function(){
    var lessonItemDtos = [];
    lessons.forEach(function(elem){
      lessonItemDtos.push({
        'id' : elem.lessonId,
        'day' : elem.day,
        'hour' : elem.hour
      });
    });
    $.ajax({
      url : '/rest/timetable/plan',
      method : 'POST',
      data :  JSON.stringify(lessonItemDtos),
      dataType : 'json',
      contentType: 'application/json; charset=utf-8',
      beforeSend: function(xhr){xhr.setRequestHeader(
         $("meta[name='_csrf_header']").attr("content"),
         $("meta[name='_csrf']").attr("content")
      );},
      success : function(data){
        $('#plan-result').text('mentés sikeres');
      },
      error : function(jqxhr, status, thrown){
        $('#plan-result').text('mentés sikertelen');
        console.log(status);
      },
    });
  });

});
