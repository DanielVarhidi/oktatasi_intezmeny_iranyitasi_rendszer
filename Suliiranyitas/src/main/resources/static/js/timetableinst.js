$(function(){

  for (var i = 8; i < 19; i++) {
    $row = $('<tr></tr>').attr("row",i)
              .append('<th>'+i+'</th>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>');
    $('#timetablebody-example').append($row);
  }

  function dayToNumber(day){
    switch (day) {
      case "MONDAY":
        return 1;
        break;
      case "TUESDAY":
        return 2;
        break;
      case "WEDNESDAY":
        return 3;
        break;
      case "THURSDAY":
        return 4;
        break;
      case "FRIDAY":
        return 5;
        break;
      default:
        return 0;
        break;
    }
  }

  function getLessonItems( func ){
    $.get(
        '/rest/timetable/inst',
        function(data){
          data.forEach(function(item){
            console.log(item);
          });

        },
        'json'
      );
  }

  function stringId(str){
    return str.replace(/[\s.]/g,'');
  }

  function lessonToTable(id, row, col, text){
    $p = $('<p></p>').text(text);
    var sel = '#' + id;
    $(sel).find('tr[row='+row+']')
        .children().eq(col)
        .append($p);
  }

  function createTable(id, name){
    $h = $('<h3></h3>').text(name);
    $('#timetables').append($h);
    $('#timetable-example').clone().attr("id", id).appendTo('#timetables').removeClass('d-none');
  }

  function clearTable(){
    $('#timetables').children().remove();
  }

  $('#byClass').click(function(){
    clearTable();
    $.get(
        '/rest/timetable/inst',
        function(data){
          data.forEach(function(item){
            var id = stringId( item.studentClassName )
            var select = '#' + id;
            if ( !$(select).length ) {
              createTable(id, item.studentClassName);
            }
            lessonToTable(id, item.hour, dayToNumber(item.day), item.name);
          });

        },
        'json'
      );
  });

  $('#byTeacher').click(function(){
    clearTable();
    $.get(
        '/rest/timetable/inst',
        function(data){
          data.forEach(function(item){
            var id = stringId( item.teacherName )
            var select = '#' + id;
            if ( !$(select).length ) {
              createTable(id, item.teacherName);
            }
            lessonToTable(id, item.hour, dayToNumber(item.day), item.name);
          });

        },
        'json'
      );
  });

});
