$(function(){
/*
//html órarend
  for (var i = 8; i < 19; i++) {
    $row = $('<tr></tr>').attr("row",i)
              .append('<th>'+i+'</th>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>');
    $('#timetablebody').append($row);
  }

  function lessonToTable(row, col, text){
    $p = $('<p></p>').text(text);
    $('#timetablebody').find('tr[row='+row+']')
        .children().eq(Number(col)+1)
        .append($p);
  }


    $('#lessonTable').find('tr').each(
      function(index){
      lessonToTable( $(this).children().eq(3).text(), $(this).children().eq(2).text(), $(this).children().eq(0).text());
    });

*/

//ajax órarend
  for (var i = 8; i < 19; i++) {
    $row = $('<tr></tr>').attr("row",i)
              .append('<th>'+i+'</th>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>')
              .append('<td></td>');
    $('#timetablebody-ajax').append($row);
  }

  function lessonToTableAjax(row, col, text){
    $p = $('<p></p>').text(text);
    $('#timetablebody-ajax').find('tr[row='+row+']')
        .children().eq(col)
        .append($p);
  }

  function dayToNumber(day){
    switch (day) {
      case "MONDAY":
        return 1;
        break;
      case "TUESDAY":
        return 2;
        break;
      case "WEDNESDAY":
        return 3;
        break;
      case "THURSDAY":
        return 4;
        break;
      case "FRIDAY":
        return 5;
        break;
      default:
        return 0;
        break;
    }
  }

  //ajax
  $.get(
      '/rest/timetable',
      function(data){
        data.forEach(function(item){
          lessonToTableAjax(item.hour, dayToNumber(item.day), item.name);
        });

      },
      'json'
    );

});
