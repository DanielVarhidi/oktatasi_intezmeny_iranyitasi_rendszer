$(function () {
    $(".datepicker").datepicker({dateFormat: "yy.mm.dd", changeMonth: true,
        changeYear: true});
    /* Új osztályzat */
    $('#registratemark-lessons').change(function () {
        if ($(this).val() != "0") {
            $.ajax({
                url: '/rest/registratemark/students',
                method: 'GET',
                data: {id: $(this).val()},
                success: function (response) {
                    $('#registratemark-student').prop('disabled', false)
                            .children().not(':first-child').remove();
                    response.forEach(function (student) {
                        var $newOption = $('<option></option>');
                        $newOption.text(student.studentName);
                        $newOption.attr('value', student.studentId);
                        $('#registratemark-student').append($newOption);
                    });
                    if ($('registratemark-student').is('[disabled=enabled]')) {
                        $('#registratemark-mark').prop('disabled', false);
                    }
                },
                xhrFields: {
                    withCredentials: true
                }
            });
        } else {
            $('#registratemark-student, #registratemark-mark').prop('disabled', true);
            $('#registratemark-student option[value="0"]').prop('selected', true);
            $('#registratemark-mark option[value="0"]').prop('selected', true);
        }
    });

    $('#registratemark-student').change(function () {
        if ($(this).val() != "0") {
            $('#registratemark-mark').prop('disabled', false);
        } else {
            $('#registratemark-mark').prop('disabled', true);
            $('#registratemark-mark option[value="0"]').prop('selected', true);
        }
    });

    $('#form').submit(function (event) {
        event.preventDefault();
        var token = $("input[name='_csrf']").val();
        if ($('#registratemark-lessons').val() === "0"
                || $('#registratemark-student').val() === "0"
                || $('#registratemark-mark').val() === "0") {
        } else {
            var addMarkDto = {};
            addMarkDto.lessonId = $('#registratemark-lessons').val();
            addMarkDto.studentId = $('#registratemark-student').val();
            addMarkDto.mark = $('#registratemark-mark').val();
            addMarkDto._csrf = token;
            $.ajax({
                url: '/rest/registratemark/newmark',
                method: 'POST',
                contentType: 'application/json',
                async: true,
                headers: {
                    'X-CSRF-TOKEN': token,
                },
                data: JSON.stringify(addMarkDto),
                success: function () {
                    alert("Osztályzat sikeresen hozzáadva!")
                }
            });
        }
    });
});

function postLogout() {
    document.getElementById("logoutform").submit();
}
