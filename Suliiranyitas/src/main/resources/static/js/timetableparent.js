$(function(){

  for (var i = 8; i < 19; i++) {
    $day1 = $('<td></td>').attr('day',"MONDAY").attr('hour',i);
    $day2 = $('<td></td>').attr('day',"TUESDAY").attr('hour',i);
    $day3 = $('<td></td>').attr('day',"WEDNESDAY").attr('hour',i);
    $day4 = $('<td></td>').attr('day',"THURSDAY").attr('hour',i);
    $day5 = $('<td></td>').attr('day',"FRIDAY").attr('hour',i);
    $row = $('<tr></tr>').attr("row",i)
              .append('<th>'+i+'</th>')
              .append($day1)
              .append($day2)
              .append($day3)
              .append($day4)
              .append($day5)
    $('#timetablebody-example').append($row);
  }

  function dayToNumber(day){
    switch (day) {
      case "MONDAY":
        return 1;
        break;
      case "TUESDAY":
        return 2;
        break;
      case "WEDNESDAY":
        return 3;
        break;
      case "THURSDAY":
        return 4;
        break;
      case "FRIDAY":
        return 5;
        break;
      default:
        return 0;
        break;
    }
  }

  function stringId(str){
    return str.replace(/[\s.]/g,'');
  }

  function lessonToTable(id, row, col, text){
    $p = $('<p></p>').text(text);
    var sel = '#' + id;
    $(sel).find('tr[row='+row+']')
        .children().eq(col)
        .append($p);
  }

  function createTable(id, studentName, instName){
    $h3 = $('<h3></h3>').text(studentName);
    $h4 = $('<h4></4>').text(instName);
    $('#timetables').append($h3).append($h4);
    $('#timetable-example').clone().attr("id", id).appendTo('#timetables').removeClass('d-none');
  }

  $.get(
      '/rest/timetable/parent',
      function(data){
        if(data.length == 0){
          $('#timetables').text('Nincs megjeleníthető órarend.');
        }
        data.forEach(function(item){
          var id = stringId( item.studentName )
          var select = '#' + id;
          if ( !$(select).length ) {
            createTable(id, item.studentName, item.institutionName);
          }
          lessonToTable(id, item.hour, dayToNumber(item.day), item.name);
        });

      },
      'json'
    );

});
