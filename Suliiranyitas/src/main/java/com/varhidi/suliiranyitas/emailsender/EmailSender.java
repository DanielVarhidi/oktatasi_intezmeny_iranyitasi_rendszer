package com.varhidi.suliiranyitas.emailsender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailSender {

    String subjectInstAdmin = "Welcome to XY! Registration as Institution Administartor";
    String subjectTeacher = "Welcome to XY! Registration as Teacher";
    String subjectStudent = "Welcome to XY! Registration as Student";
    String subjectParent = "Welcome to XY! Registration as Parent";


    
    String textTeacher = "<h2>Tisztelt Pedagógus!</h2><br>"
            + "<p>Örömmel értesítjük, hogy meghívást kapott a XY rendszerhez, amely megkönnyíti munkáját.</p><br>"
            + "<p>Kérem az alábbi linken regisztráljon felületünkre:</p><br>"
            + "<a href=\"https://www.XY.com\"><button>Tovább a XY-ra</button></a><br>"
            + "<br>"
            + "<p>XY csapata</p><br>"
            + "<p>Ügyfélszolgálat</p><br>"
            + "<p>+3670 123 45 67</p>";
    
    String textStudent = "<h2>Tisztelt Felhasználó!</h2><br>"
            + "<p>Örömmel értesítjük, hogy meghívást kapott a XY rendszerhez, amely megkönnyíti munkáját.</p><br>"
            + "<p>Kérem az alábbi linken regisztráljon felületünkre:</p><br>"
            + "<a href=\"https://www.XY.com\"><button>Tovább a XY-ra</button></a><br>"
            + "<br>"
            + "<p>XY csapata</p><br>"
            + "<p>Ügyfélszolgálat</p><br>"
            + "<p>+3670 123 45 67</p>";
    
    String textParent = "<h2>Kedves Tanuló!</h2><br>"
            + "<p>Örömmel ártesítelek, hogy meghívást kaptál a XY rendszerhez, amely megkönnyíti tanulásodat.</p><br>"
            + "<p>Az alábbi linken tudsz regisztrálni felületünkre:</p><br>"
            + "<a href=\"https://www.XY.com\"><button>Tovább a XY-ra</button></a><br>"
            + "<br>"
            + "<p>XY csapata</p><br>"
            + "<p>Ügyfélszolgálat</p><br>"
            + "<p>+3670 123 45 67</p>";


    @Autowired
    public JavaMailSender emailSender;

    /**
     * Send a simple email
     *
     * @param to
     * @param subject
     * @param text
     */
    public void sendSimpleMessage(String to, String subject, String text) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new  MimeMessageHelper(message);
        helper.setTo(to);
        helper.setSubject(subject);
        
        message.setContent(text, "text/html");
        
        emailSender.send(message);


    }

    public void sendInstAdminRegMessage(String to,String random,String randomPassword) throws MessagingException {
        String regPath="http://localhost:8080/login";
        String fullPath=regPath+random;
        String textInstAdmin = "<h2>Tisztelt Felhasználó!</h2><br>"
        + "<p>Örömmel értesítjük, hogy meghívást kapott az iskola írányítási rendszereünkhöz, amely megkönnyíti munkáját.</p><br>"
        + "<p>Kérem az alábbi linken regisztráljon felületünkre:</p><br>"
        + "<a href=\""+regPath+"\"><p>Regisztrálok</p></a><br>"
        + "<p>Email címével és az ideiglenes jelszóval tud bejelentkezni.</p><br>"
        + "<p>Ideiglenes jelszava: "+randomPassword+"</p><br>"    
        + "<br>"
        + "<p>XY csapata</p><br>"
        + "<p>Ügyfélszolgálat</p><br>"
        + "<p>+3670 123 45 67</p>";
        
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new  MimeMessageHelper(message);
        helper.setTo(to);
        
        helper.setSubject(subjectInstAdmin);
        
        message.setContent(textInstAdmin, "text/html");
        
        emailSender.send(message);

    }

    public void sendTeacherRegMessage(String to, String text) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new  MimeMessageHelper(message);
        helper.setTo(to);
        helper.setSubject(subjectTeacher);
        
        message.setContent(textTeacher, "text/html");
        
        emailSender.send(message);

    }

    public void sendStudentRegRegMessage(String to, String text) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new  MimeMessageHelper(message);
        helper.setTo(to);
        helper.setSubject(subjectStudent);
        
        message.setContent(textStudent, "text/html");
        
        emailSender.send(message);

    }

    public void sendParentRegRegMessage(String to, String text) throws MessagingException {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new  MimeMessageHelper(message);
        helper.setTo(to);
        helper.setSubject(subjectParent);
        
        message.setContent(textParent, "text/html");
        
        emailSender.send(message);

    }
}
