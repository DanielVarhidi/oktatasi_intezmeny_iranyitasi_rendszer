/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.ClassDto;
import com.varhidi.suliiranyitas.dtos.MessageDto;
import com.varhidi.suliiranyitas.services.MessageService;
import com.varhidi.suliiranyitas.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Áron
 */
@RestController
@RequestMapping("/rest/forum")
public class ForumRestController {

    @Autowired
    MessageService ms;

    @Autowired
    UserService us;

    @RequestMapping(path = "/messages", method = RequestMethod.GET, produces = "application/json")
    public List<MessageDto> findAllMessages() {
        return ms.findMessagesOfClass();
    }

    @RequestMapping(path = "/newmessage", method = RequestMethod.POST, produces = "application/json")
    public MessageDto addNewMessage(@RequestBody MessageDto messageDto) {
        messageDto.setAuthor(us.getCurrentUser().getName());
        ms.addNewMessage(messageDto);
        return messageDto;
    }

    @RequestMapping(path = "/selectclass", method = RequestMethod.GET, produces = "application/json")
    public List<ClassDto> findClassesOfTeacher() {
        return ms.findAllClassOfEachTeacher();
    }

    @RequestMapping(path = "/classmessages/{id}", method = RequestMethod.GET, produces = "application/json")
    public List<MessageDto> findAllMessages(@PathVariable(name = "id") Long id) {
        return ms.findMessagesByClass(id);
    }

}
