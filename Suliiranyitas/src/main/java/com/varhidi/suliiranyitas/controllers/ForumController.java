/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Áron
 */
@Controller
public class ForumController {

    @Autowired
    UserService us;

    @RequestMapping(path = "/forum/student", method = RequestMethod.GET)
    public String getTheMessagesForStudents(Model model) {
        model.addAttribute("username", us.getCurrentUser().getName());
        return "classForum";
    }

    @RequestMapping(path = "/forum/teacher", method = RequestMethod.GET)
    public String getTheMessagesForTeachers(Model model) {
        model.addAttribute("username", us.getCurrentUser().getName());
        return "teacherforum";
    }

}
