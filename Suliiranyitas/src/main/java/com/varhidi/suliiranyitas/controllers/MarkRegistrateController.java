/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.AddMarkDto;
import com.varhidi.suliiranyitas.dtos.LessonDto;
import com.varhidi.suliiranyitas.services.MarkRegistrateService;
import com.varhidi.suliiranyitas.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Áron
 */
@Controller
public class MarkRegistrateController {

    @Autowired
    MarkRegistrateService markRegServ;

    @Autowired
    UserService userService;

    @RequestMapping(path = "/regmark", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('TEACHER')")
    public String getAllLessonsOfTeacher(Model model) {
        List<LessonDto> lessonDtos = markRegServ.getAllLessonsOfTeacher(userService.getCurrentUser());
        AddMarkDto addMarkDto = new AddMarkDto();
        model.addAttribute("lessonDtos", lessonDtos);
        model.addAttribute("addMarkDto", addMarkDto);
        return "registrateMark";
    }
}
