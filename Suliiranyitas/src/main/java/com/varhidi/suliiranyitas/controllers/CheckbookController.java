/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.StudentResultDto;
import com.varhidi.suliiranyitas.services.StudentResultService;
import com.varhidi.suliiranyitas.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Áron
 */
@Controller
public class CheckbookController {

    @Autowired
    StudentResultService studResServ;

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/mystats"}, method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('STUDENT')")
    public String showMyMarks(Model m) {
        List<StudentResultDto> resultsOfStudent = studResServ.getResultsForStudentByLesson(userService.getCurrentUser());
        m.addAttribute("studResDto", resultsOfStudent);
        return "checkbookstudent";
    }

}
