
package com.varhidi.suliiranyitas.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Eszter
 */

@Controller
public class LoginController {
    
    @RequestMapping(method = RequestMethod.GET, path = "/login")
    public String trial(){
        return "login";
    }
    
}
