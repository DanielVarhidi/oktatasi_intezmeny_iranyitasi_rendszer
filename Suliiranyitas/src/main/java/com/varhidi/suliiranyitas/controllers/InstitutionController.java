package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.InstitutionDto;
import com.varhidi.suliiranyitas.services.InstitutionService;
import java.util.List;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author DaniKiss
 */
@Controller
public class InstitutionController {

    @Autowired
    InstitutionService institutionService;

    @RequestMapping(value = {"/ourschools"}, method = RequestMethod.GET)
    //@PreAuthorize("hasAnyAuthourity('GLOBAL_ADMINISTRATOR', 'LOCAL_ADMINISTRATOR')")
    public String showInstitutions(Model m) {
        List<InstitutionDto> allInstitutions = institutionService.listAllInstitution();
        m.addAttribute("institutionList", allInstitutions);
        return "ourschools";
    }

    @RequestMapping(value = {"/institutionreg"}, method = RequestMethod.GET)
    public String institutionreg(Model m) {
        m.addAttribute("institution", institutionService.newInstitutionDto());
        return "institutionreg";
    }

    @PostMapping(value = "/add")
    public String addInstitutionToDataBase(@ModelAttribute InstitutionDto institution) throws MessagingException {

        institutionService.addInstitutionIntoTheDataBase(institution);
        return "redirect:/ourschools";
    }

    @RequestMapping(value = {"/institutionprofile/{code}"}, method = RequestMethod.GET)
    public String showInstitutionProfile(Model m, @PathVariable(name = "code") Long id) {
        InstitutionDto temp = institutionService.findInstitutionById(id);
        m.addAttribute("institution", temp);

        return "institutionprofile";
    }
}
