/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.config.WebSecurityConfig;
import com.varhidi.suliiranyitas.dtos.RegDto;
import com.varhidi.suliiranyitas.dtos.UserDto;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.Role;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.entities.UserRole;
import com.varhidi.suliiranyitas.services.RoleService;
import com.varhidi.suliiranyitas.services.UserService;
import java.security.Principal;
import java.util.ArrayList;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author István
 */
//TODO refactor em calls to service layer
@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @PersistenceContext
    EntityManager em;

    @RequestMapping(method = RequestMethod.GET, path = "/registerstudent")
    public String student(Model model, Principal principal) {
        UserDto userDto = userService.getCurrentUserDto();
        model.addAttribute("user", userDto);
        return "studentRegistration";
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, path = "/registerstudent")
    public String registerStudent(@ModelAttribute UserDto user, Model model, Principal principal, BindingResult res) {
        if (res.hasErrors()) {
            model.addAttribute("user", user);
            return "registerstudent";
        }

        User temp = userService.getUserByUserName(principal.getName());
        user.setId(temp.getId());
        user.setUserName(temp.getUserName());
        user.setPassword(WebSecurityConfig.passwordEncoder().encode(user.getPassword()));
        userService.update(user);

        Institution institution = temp.getRoles().get(0).getInstitute();
        em.remove(temp.getRoles().get(0));
        ArrayList<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setRole(UserRole.STUDENT);
        role.setUser(temp);
        role.setInstitute(institution);
        roles.add(role);
        temp.setRoles(roles);
        em.persist(role);
        temp.setActivation("");
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/registerteacher")
    public String teacher(Model model, Principal principal) {
        UserDto userDto = userService.getCurrentUserDto();
        model.addAttribute("user", userDto);
        return "teacherRegistration";
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, path = "/registerteacher")
    public String registerTeacher(@ModelAttribute UserDto user, Model model, Principal principal, BindingResult res) {
        if (res.hasErrors()) {
            model.addAttribute("user", user);
            return "registerteacher";
        }

        User temp = userService.getUserByUserName(principal.getName());
        user.setId(temp.getId());
        user.setUserName(temp.getUserName());
        user.setPassword(WebSecurityConfig.passwordEncoder().encode(user.getPassword()));
        userService.update(user);

        Institution institution = temp.getRoles().get(0).getInstitute();
        em.remove(temp.getRoles().get(0));
        ArrayList<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setRole(UserRole.TEACHER);
        role.setUser(temp);
        role.setInstitute(institution);
        roles.add(role);
        temp.setRoles(roles);
        em.persist(role);
        temp.setActivation("");
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/registerparent")
    public String parent(Model model, Principal principal) {
        UserDto userDto = userService.getCurrentUserDto();
        model.addAttribute("user", userDto);
        return "parentRegistration";
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, path = "/registerparent")
    public String registerParent(@ModelAttribute UserDto user, Model model, Principal principal, BindingResult res) {
        if (res.hasErrors()) {
            model.addAttribute("user", user);
            return "registerparent";
        }

        User temp = userService.getUserByUserName(principal.getName());
        user.setId(temp.getId());
        user.setUserName(temp.getUserName());
        user.setPassword(WebSecurityConfig.passwordEncoder().encode(user.getPassword()));
        userService.update(user);

        Institution institution = temp.getRoles().get(0).getInstitute();
        em.remove(temp.getRoles().get(0));
        ArrayList<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setRole(UserRole.PARENT);
        role.setUser(temp);
        role.setInstitute(institution);
        roles.add(role);
        temp.setRoles(roles);
        em.persist(role);
        temp.setActivation("");
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/registerlocaladmin")
    public String localAdmin(Model model, Principal principal) {
        UserDto userDto = userService.getCurrentUserDto();
        model.addAttribute("user", userDto);
        return "localAdminRegistration";
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, path = "/registerlocaladmin")
    public String registerLocalAdmin(@ModelAttribute UserDto user, Model model, Principal principal, BindingResult res) {
        if (res.hasErrors()) {
            model.addAttribute("user", user);
            return "localAdminRegistration";
        }

        User temp = userService.getUserByUserName(principal.getName());
        user.setId(temp.getId());
        user.setUserName(temp.getUserName());
        user.setPassword(WebSecurityConfig.passwordEncoder().encode(user.getPassword()));
        userService.update(user);

        Institution institution = temp.getRoles().get(0).getInstitute();
        em.remove(temp.getRoles().get(0));
        ArrayList<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setRole(UserRole.LOCAL_ADMINISTRATOR);
        role.setUser(temp);
        role.setInstitute(institution);
        roles.add(role);
        temp.setRoles(roles);
        em.persist(role);
        temp.setActivation("");
        return "index";
    }

    @RequestMapping(value = {"/addTeacher"}, method = RequestMethod.GET)
    public String addTeacher(Model m, Principal principal, RegDto regDto) {
        m.addAttribute("regDto", regDto);
        return "addTeacher";
    }

    @PostMapping(value = "/addTeacher")
    public String addTeacher(@ModelAttribute RegDto regDto, Principal principal, Model model) throws MessagingException {

        Institution institution = userService.getCurrentRole().getInstitute();
        userService.saveTeacher(institution, regDto.getEmail());
        return "index";
    }

    @RequestMapping(value = {"/addStudent"}, method = RequestMethod.GET)
    public String addStudent(Model m, Principal principal, RegDto regDto) {
        m.addAttribute("regDto", regDto);
        return "addStudent";
    }

    @PostMapping(value = "/addStudent")
    public String addStudent(@ModelAttribute RegDto regDto, Principal principal, Model model) throws MessagingException {

        Institution institution = userService.getCurrentRole().getInstitute();
        userService.saveStudent(institution, regDto.getEmail());
        return "index";
    }

    @RequestMapping(value = {"/addParent"}, method = RequestMethod.GET)
    public String addParent(Model m, Principal principal, RegDto regDto) {
        m.addAttribute("regDto", regDto);
        return "addParent";
    }

    @PostMapping(value = "/addParent")
    public String addParent(@ModelAttribute RegDto regDto, Principal principal, Model model) throws MessagingException {

        Institution institution = userService.getCurrentRole().getInstitute();
        userService.saveParent(institution, regDto.getEmail());
        return "index";
    }

    @RequestMapping(value = {"/addLocalAdmin"}, method = RequestMethod.GET)
    public String addLocalAdmin(Model m, Principal principal, RegDto regDto) {

        m.addAttribute("regDto", regDto);
        return "addLocalAdmin";
    }

    @PostMapping(value = "/addLocalAdmin")
    public String addLocalAdmin(@ModelAttribute RegDto regDto, Principal principal, Model model) throws MessagingException {

        Institution institution = userService.getCurrentRole().getInstitute();
        userService.saveLocalAdmin(institution, regDto.getEmail());
        return "index";
    }
}
