/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.AddMarkDto;
import com.varhidi.suliiranyitas.dtos.AddMarkStudentDto;
import com.varhidi.suliiranyitas.services.MarkRegistrateService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Áron
 */
@RestController
@RequestMapping("/rest/registratemark")
public class MarkRegistrateRestController {

    @Autowired
    MarkRegistrateService markRegServ;

    @RequestMapping(path = "/students", method = RequestMethod.GET, produces = "application/json")
    public List<AddMarkStudentDto> findAllStudentsOfLesson(Long id) {
        return markRegServ.getAllStudentsOfLesson(id);
    }

    @RequestMapping(path = "/newmark", method = RequestMethod.POST, produces = "application/json")
    public AddMarkDto addNewMark(@RequestBody AddMarkDto addMarkDto) {
        markRegServ.persistNewMark(addMarkDto.getStudentId(), addMarkDto.getLessonId(), addMarkDto.getMark());
        return addMarkDto;
    }
}
