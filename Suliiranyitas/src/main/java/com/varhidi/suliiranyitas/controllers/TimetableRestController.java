package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.LessonItemDto;
import com.varhidi.suliiranyitas.dtos.TimetableDto;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.services.OurUserDetails;
import com.varhidi.suliiranyitas.services.TimetableService;
import com.varhidi.suliiranyitas.services.UserService;
import com.varhidi.suliiranyitas.timetableplanner.Problem;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.varhidi.suliiranyitas.timetableplanner.TimetablePlanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Kovács Dani
 */
@RestController
@RequestMapping("/rest/timetable")
public class TimetableRestController {
    
    @Autowired
    UserService us;
    
    @Autowired
    UserDetailsService uds;
    
    @Autowired
    TimetableService timetableService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TimetableRestController.class);
    
   
    @RequestMapping(method = RequestMethod.GET, path = "/common")
    public List<TimetableDto> timetableCommon(){
        String userName = us.getCurrentUser().getUserName();
        OurUserDetails oud = (OurUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = oud.getCurrentAuth().getAuthority();
        List<TimetableDto> timetableDtos = new ArrayList<>();
        switch(role){
            case "PARENT":
                timetableDtos = timetableService.getParentTimetableByUsername(userName);
                break;
            case "STUDENT":
                timetableDtos = timetableService.getStudentTimetableByUsername(userName);
                break;
            case "TEACHER":
                timetableDtos = timetableService.getTeacherTimetableByUsername(userName);
                break;
            default:
                break;
        }
        return timetableDtos;
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/inst")
    @PreAuthorize("hasAuthority('LOCAL_ADMINISTRATOR')")
    public List<LessonItemDto> timetableLocalAdmin(Model model, Principal principal){
        OurUserDetails oud = (OurUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Institution inst = oud.getInstitution();
        Long id = inst.getId();
        List<LessonItemDto> items = timetableService.getLessonItemsByInstitutionId( id );
        return items;
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/plan")
    @PreAuthorize("hasAuthority('LOCAL_ADMINISTRATOR')")
    public List<LessonItemDto> timetablePlanner(Model model, Principal principal){
        LOGGER.info("timetablePlanner started");
        OurUserDetails oud = (OurUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Institution inst =  timetableService.getInstitutionById(oud.getInstitution().getId());
        Problem solved = TimetablePlanner.plan(inst);
        List<LessonItemDto> items = new ArrayList<>();
        solved.getLessons().forEach(dli -> 
                items.add(new LessonItemDto(
                        dli.getId(),
                        dli.getHour().getDay(),
                        dli.getHour().getHour(),
                        dli.getTeacher().getId(),
                        dli.getStudentClass().getId())
        ));
        LOGGER.info("timetablePlanner finished");
        return items;
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/plan")
    @PreAuthorize("hasAuthority('LOCAL_ADMINISTRATOR')")
    public Boolean timetableSave( @RequestBody List<LessonItemDto> lessonItems){
        //Institution inst = us.getCurrentUser().getRoles().get(0).getInstitute();
        timetableService.updateTimetable(lessonItems);
        
        return true;
    }


}
