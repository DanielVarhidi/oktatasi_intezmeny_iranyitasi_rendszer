package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.dtos.RoleDto;
import com.varhidi.suliiranyitas.dtos.UserDto;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.entities.UserRole;
import com.varhidi.suliiranyitas.services.MainService;
import com.varhidi.suliiranyitas.services.UserService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {



    @Autowired
    private UserService us;
    
    @Autowired
    private MainService mainService;

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String showLandingPage() {
        User user = us.getCurrentUser();
        if (user == null) {
            return "publicLandingPage";
        } else {
            return "index";
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/index")
    public String showIndexhtml(Principal principal) {
        User user = us.getUserByUserName(principal.getName());
        if (!user.getEnabled()) {
            if (user.getActivation().contains(UserRole.TEMP_ADMINISTRATOR.toString())) {
                return "redirect:registerlocaladmin";
            } else if (user.getActivation().contains(UserRole.TEMP_PARENT.toString())) {
                return "redirect:registerparent";
            } else if (user.getActivation().contains(UserRole.TEMP_STUDENT.toString())) {
                return "redirect:registerstudent";
            } else if (user.getActivation().contains(UserRole.TEMP_TEACHER.toString())) {
                return "redirect:registerteacher";
            }
        } else {
            return "index";
        }
        return "index";
    }

    //myprofile.html
    @RequestMapping(method = RequestMethod.GET, path = "/myprofile")
    public String profile(Model model, Principal principal) {
        User user = us.getUserByUserName(principal.getName());
        model.addAttribute("user", user);
        return "myprofile";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/editprofile")
    public String editProfile(Model model, Principal principal) {
        //User user = us.getUserByUserName(principal.getName());
        UserDto userDto = us.getCurrentUserDto();
        model.addAttribute("userDto", userDto);
        return "editprofile";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/editprofile")
    public String editProfileSave(@ModelAttribute UserDto userDto, Model model, Principal principal) {
        //us.save(user);
        User user = us.getUserByUserName(principal.getName());
        userDto.setId(user.getId());
        userDto.setUserName(user.getUserName());
        us.update(userDto);
        return "redirect:/myprofile";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/rolechange")
    public String roleChange(Model model){
        model.addAttribute("roles",  mainService.getRoles(us.getCurrentUser().getUserName()) );
        model.addAttribute("roleDto", new RoleDto() );
        return "rolechange";
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/rolechange")
    public String roleChangePost( @ModelAttribute("roleDto") RoleDto roleDto){
        mainService.changeRole(roleDto.getId());
        return "index";
    }
}
