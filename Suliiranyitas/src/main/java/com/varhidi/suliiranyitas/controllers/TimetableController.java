package com.varhidi.suliiranyitas.controllers;

import com.varhidi.suliiranyitas.services.TimetableService;
import com.varhidi.suliiranyitas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**órarendhez
 *
 * @author Kovács Dani
 */
@Controller
public class TimetableController {
    
    @Autowired
    UserService us;
    
    @Autowired
    TimetableService timetableService;
    
    @RequestMapping(method = RequestMethod.GET, path = "/timetable/inst")
    @PreAuthorize("hasAuthority('LOCAL_ADMINISTRATOR')")
    public String timetableInst(){
        return "timetableinst";
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/timetable/plan")
    @PreAuthorize("hasAuthority('LOCAL_ADMINISTRATOR')")
    public String timetablePlan(){
        return "timetableplan";
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/timetable/common")
    public String timetableCommon(){
        return "timetablecommon";
    }
    
}
