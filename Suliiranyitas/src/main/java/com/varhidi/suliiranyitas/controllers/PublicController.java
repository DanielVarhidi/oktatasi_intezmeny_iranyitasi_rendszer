package com.varhidi.suliiranyitas.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PublicController {

    @RequestMapping(path = "/home", method = RequestMethod.GET)
    public String showPublicLandingPage() {
        return "publicLandingPage";
    }
    
    @RequestMapping(path = "/sendamessage", method = RequestMethod.GET)
    public String showPublicbasicSales() {
        return "basicSales";
    }
}
