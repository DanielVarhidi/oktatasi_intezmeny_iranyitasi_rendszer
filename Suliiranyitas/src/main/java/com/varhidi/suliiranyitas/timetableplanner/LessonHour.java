package com.varhidi.suliiranyitas.timetableplanner;

import com.varhidi.suliiranyitas.entities.Day;

/**
 *
 * @author Kovács Dani
 */
public class LessonHour {

    private int hour;
    private Day day;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }
    
    public int getWeeksHour(){
        return day.ordinal()*24 + hour;
    }
}
