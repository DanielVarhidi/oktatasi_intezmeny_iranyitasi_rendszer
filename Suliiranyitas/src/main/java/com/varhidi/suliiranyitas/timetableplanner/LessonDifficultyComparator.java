package com.varhidi.suliiranyitas.timetableplanner;

import java.util.Comparator;

/**
 *
 * @author Kovács Dani
 */
public class LessonDifficultyComparator implements Comparator<DetailedLessonItem>{

    public LessonDifficultyComparator() {
    }
    
    @Override
    public int compare(DetailedLessonItem o1, DetailedLessonItem o2) {
        return o1.getTeacher().getLessons().size() - o2.getTeacher().getLessons().size();
    }
    
    
}
