package com.varhidi.suliiranyitas.timetableplanner;

import java.util.HashSet;
import java.util.Set;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

/**
 *
 * @author Kovács Dani
 */
public class LessonEasyScoreCalculator implements EasyScoreCalculator<Problem>{

    @Override
    public HardSoftScore calculateScore(Problem problem) {
        int hardScore=0, softScore=0;
        
        Set<LessonHour> hours = new HashSet<>();
        
        //brute force
        for (DetailedLessonItem lesson : problem.getLessons()) {
            for (DetailedLessonItem other : problem.getLessons()) {
                if( !lesson.equals(other) ){
                    if( lesson.getHour() == other.getHour() && lesson.getTeacher() == other.getTeacher() ||
                        lesson.getHour() == other.getHour() && lesson.getStudentClass() == other.getStudentClass()    ){
                        hardScore--;
                    }
                }
            }
            hours.add(lesson.getHour());
            softScore -= hourWeight(lesson.getHour());
        }
        softScore -= hours.size();
        
        
        
        return HardSoftScore.valueOf(hardScore, softScore);
    }
    
    public static int hourWeight(LessonHour lh){
        if(lh==null){
            return 1;
        }
        return lh.getHour() - 7;
    }
}