package com.varhidi.suliiranyitas.timetableplanner;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kovács Dani
 */
public class SClass {

    private Long id;
    private List<DetailedLessonItem> lessons = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<DetailedLessonItem> getLessons() {
        return lessons;
    }

    public void setLessons(List<DetailedLessonItem> lessons) {
        this.lessons = lessons;
    }
    
    public void addLesson(DetailedLessonItem lesson){
        lessons.add(lesson);
        lesson.setStudentClass(this);
    }
    
    public void removeLesson(DetailedLessonItem lesson){
        lessons.remove(lesson);
        lesson.setStudentClass(null);
    }
}
