package com.varhidi.suliiranyitas.timetableplanner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kovács Dani
 */
@PlanningSolution
public class Problem implements Solution<HardSoftScore> {
    
    private List<Teacher> teachers;
    private List<SClass> studentClasses;
    private List<DetailedLessonItem> lessons;
    private List<LessonHour> hours;
    
    private HardSoftScore score;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Problem.class);
    
    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public List<SClass> getStudentClasses() {
        return studentClasses;
    }

    public void setStudentClasses(List<SClass> studentClasses) {
        this.studentClasses = studentClasses;
    }

    @PlanningEntityCollectionProperty
    public List<DetailedLessonItem> getLessons() {
        return lessons;
    }

    public void setLessons(List<DetailedLessonItem> lessons) {
        this.lessons = lessons;
    }

    @ValueRangeProvider(id = "hourRange")
    public List<LessonHour> getHours() {
        return hours;
    }

    public void setHours(List<LessonHour> hours) {
        this.hours = hours;
    }

    public void print(){
        if (!LOGGER.isDebugEnabled()) {
            return;
        }
        LOGGER.debug("--- hard : " + score.getHardScore() + ", soft : " +
                score.getSoftScore() + " ---");
        for (DetailedLessonItem lesson : lessons) {
            LOGGER.trace(
                "class:" + lesson.getStudentClass().getId() +
                ", teacher:" + lesson.getTeacher().getId() +
                ", hour:" + lesson.getHour().getDay() +
                " " + lesson.getHour().getHour()
            );
        }
    }
    
    @Override
    public HardSoftScore getScore() {
        return score;
    }

    @Override
    public void setScore(HardSoftScore s) {
        score = s;
    }

    @Override
    public Collection<? extends Object> getProblemFacts() {
        List<Object> facts = new ArrayList<Object>();
        facts.addAll(teachers);
        facts.addAll(hours);
        facts.addAll(studentClasses);
        // Do not add the planning entity's (processList) because that will be done automatically
        return facts;
    }
}
