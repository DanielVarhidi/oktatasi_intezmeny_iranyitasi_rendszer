package com.varhidi.suliiranyitas.timetableplanner;

import com.varhidi.suliiranyitas.entities.Institution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

/**
 *
 * @author User
 */
public class TimetablePlanner {
    
    public static Problem plan(Institution inst){
        SolverFactory solverFactory = SolverFactory.createFromXmlResource(
                    "TimetableSolverConfig.xml");
        Solver solver = solverFactory.buildSolver();

        ProblemLoader p = new ProblemLoader(inst);
        Problem unsolvedProblem = p.load();
        
        solver.solve(unsolvedProblem);
        Problem solvedProblem = (Problem) solver.getBestSolution();
    
        solvedProblem.print();
        
        return solvedProblem;
    }
}
