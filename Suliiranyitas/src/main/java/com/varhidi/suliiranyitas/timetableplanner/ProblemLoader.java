package com.varhidi.suliiranyitas.timetableplanner;

import com.varhidi.suliiranyitas.entities.Day;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.Lesson;
import com.varhidi.suliiranyitas.entities.LessonItem;
import com.varhidi.suliiranyitas.entities.Role;
import com.varhidi.suliiranyitas.entities.StudentClass;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.entities.UserRole;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Kovács Dani
 */
public class ProblemLoader {
    
    private Institution inst;

    public ProblemLoader(Institution inst) {
        this.inst = inst;
    }
    
    public Problem load(){
        Problem problem = new Problem();
    
        List<Teacher> teachers = new ArrayList<>();
        List<SClass> studentClasses = new ArrayList<>();
        List<LessonHour> hours = new ArrayList<>();
        List<DetailedLessonItem> lessons = new ArrayList<>();
        
        Map<Long, Teacher> teacherMap = new TreeMap<>();
        Map<Long, SClass> classMap = new TreeMap<>();
        
        for (Role member : inst.getMembers()) {
            if(member.getRole().equals(UserRole.TEACHER)){
                Teacher teacher = new Teacher();
                teacher.setId(member.getUser().getId());
                teachers.add(teacher);
                teacherMap.put(teacher.getId(), teacher);
            }
        }

        for (StudentClass studentClass : inst.getStudentClasses()) {
            SClass sc = new SClass();
            sc.setId(studentClass.getId());
            studentClasses.add(sc);
            classMap.put(sc.getId(), sc);
        }
        
        for (int i = 8; i < 19; i++) {
            for (int j = 0; j < 5; j++) {
                LessonHour lh = new LessonHour();
                lh.setHour(i);
                lh.setDay(Day.values()[j]);
                hours.add(lh);
            }
        }
        
        for (Lesson l : inst.getLessons()) {
            for (LessonItem lessonItem : l.getLessonItems()) {
                DetailedLessonItem dli = new DetailedLessonItem();
                SClass sc =  classMap.get( l.getStudents().get(0).getStudentClass().getId() );
                sc.addLesson(dli);
                //dli.setStudentClass(sc);
                Teacher teacher = teacherMap.get( l.getTeacher().getId() );
                teacher.addLesson(dli);
                //dli.setTeacher(teacher);
                dli.setId(lessonItem.getId());
                lessons.add(dli);
            }
            
        }
        
        problem.setHours(hours);
        problem.setLessons(lessons);
        problem.setStudentClasses(studentClasses);
        problem.setTeachers(teachers);
        
        return problem;
    }
}
