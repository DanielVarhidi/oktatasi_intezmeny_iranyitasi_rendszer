package com.varhidi.suliiranyitas.timetableplanner;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

/**
 *
 * @author Kovács Dani
 */
@PlanningEntity(difficultyComparatorClass = LessonDifficultyComparator.class)
public class DetailedLessonItem {
    
    private Long id;
    private SClass studentClass;
    private Teacher teacher;
    private LessonHour hour;

    public SClass getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(SClass studentClass) {
        this.studentClass = studentClass;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @PlanningVariable(valueRangeProviderRefs = {"hourRange"})
    public LessonHour getHour() {
        return hour;
    }

    public void setHour(LessonHour hour) {
        this.hour = hour;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
