/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.service.autoDao;

import com.varhidi.suliiranyitas.entities.Lesson;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Áron
 */
public interface LessonAutoDao extends JpaRepository<Lesson, Long> {

    List<Lesson> findByStudents_UserName(String userName);

    List<Lesson> findByTeacher_UserName(String userName);

}
