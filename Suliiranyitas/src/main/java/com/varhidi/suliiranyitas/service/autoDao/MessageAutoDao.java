/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.service.autoDao;

import com.varhidi.suliiranyitas.entities.Message;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Áron
 */
public interface MessageAutoDao extends JpaRepository<Message, Long> {

    List<Message> findByStudentClassId(Long studentClass);

}
