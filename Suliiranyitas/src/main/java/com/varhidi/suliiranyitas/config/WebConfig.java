package com.varhidi.suliiranyitas.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Dani
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/home").setViewName("publicLandingPage");        
        registry.addViewController("/sendamessage").setViewName("basicSales");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }
    
    
    
}
