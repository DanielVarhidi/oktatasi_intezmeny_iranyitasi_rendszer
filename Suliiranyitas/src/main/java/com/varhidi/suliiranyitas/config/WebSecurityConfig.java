package com.varhidi.suliiranyitas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
/*ez a két plusz anotáció nélkül nem müködött rendesen a jögkörök autentikácioja*/
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .exceptionHandling().accessDeniedHandler(accessDeniedHandler())
//                .and()
                .authorizeRequests()
//                .antMatchers("/ourschools").hasAuthority("GLOBAL_ADMINISTRATOR")
//                .antMatchers("/ourschools").hasAuthority("LOCAL_ADMINISTRATOR")
//                .antMatchers("/ourschools").hasAuthority("TEACHER")
//                .antMatchers("/ourschools").hasAuthority("STUDENT")
//                .antMatchers("/ourschools").hasAuthority("PARENT")
                .antMatchers("/").permitAll()
                .antMatchers("/sendamessage").permitAll()
                .antMatchers("/institutionreg/*").denyAll()
                /*elöször tiltottam az összes az aloldra írányuló kérést, majd alább
                eggyenként engedélyezem a hozzá férést a megfelelő személyeknek a megfelelő aloldalhoz
                vagy az aloldal alatti aloldalakhoz.*/
                .antMatchers("/institutionreg").hasAuthority("GLOBAL_ADMINISTRATOR")
                
                .antMatchers("/css/*", "/js/*", "/img/*", "/scss/*", "/mp4/*", "/vendor/**", "/images/blackboard.jpg").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/index")
                .and()
                .logout()
                .logoutSuccessUrl("/login?logout")
                .permitAll();
    }

}
