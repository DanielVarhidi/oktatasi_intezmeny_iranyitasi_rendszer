package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Az egyes iskolai osztályokhoz kapcsolódó (fórum) üzenetek.
 *
 * @author Kovács Dániel
 */
@Entity
public class Message extends BaseEntity implements Serializable {

    private String author;

    private String text;

    @ManyToOne
    private StudentClass studentClass;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public StudentClass getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(StudentClass studentClass) {
        this.studentClass = studentClass;
    }

}
