package com.varhidi.suliiranyitas.entities;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Kovács Dániel;
 */
public enum UserRole implements GrantedAuthority {
    GLOBAL_ADMINISTRATOR,
    LOCAL_ADMINISTRATOR,
    TEACHER,
    STUDENT,
    PARENT,
    TEMP_ADMINISTRATOR,
    TEMP_TEACHER,
    TEMP_STUDENT,
    TEMP_PARENT;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
