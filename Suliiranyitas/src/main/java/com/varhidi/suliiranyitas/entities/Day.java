package com.varhidi.suliiranyitas.entities;

/** A hét napjai.
 *
 * @author Kovács Dániel
 */
public enum Day {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
}
