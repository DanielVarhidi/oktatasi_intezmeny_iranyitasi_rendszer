package com.varhidi.suliiranyitas.entities;

import com.varhidi.suliiranyitas.commons.DateConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Az általános user.
 *
 * @author Kovács Dániel
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "userWithRolesAndClass",
            attributeNodes = {
                @NamedAttributeNode(value = "roles")
                ,
                @NamedAttributeNode(value = "studentClass")
            })
})
@Table(name = "schooluser")
public class User extends BaseEntity implements Serializable {

    @Column(unique = true, nullable = false)
    @Size(max = 50)
    private String userName;

    private String password;

    private boolean isGlobalAdmin = false;

    @Size(max = 50)
    private String name;

    @Size(max = 50)
    private String address;

    @Size(max = 50)
    private String emailAddress;

    @Size(max = 50)
    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+")
    private String phoneNumber;

    //@Column(unique = true, nullable = false)
    @Size(max = 50)
    private String personalIdNumber;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = DateConstants.DATE_PATTERN)
    private Date birthDate;

    @Size(max = 50)
    private String birthPlace;

    @Size(max = 50)
    private String mothersName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Role> roles = new ArrayList<>();

    //family
    @ManyToMany
    @JoinTable(name = "familyrelations")
    private List<User> parents = new ArrayList<>();

    @ManyToMany(mappedBy = "parents")
    private List<User> children = new ArrayList<>();

    //qualification
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Qualification> qualifications = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Subject> subjects = new ArrayList<>();

    @ManyToOne
    private StudentClass studentClass;

    //tanárok órái
    @OneToMany(mappedBy = "teacher", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Lesson> teachersLessons = new ArrayList<>();

    //diákok órái
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "students_lessons")
    private List<Lesson> studentsLessons = new ArrayList<>();

    //osztályzatok
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Mark> marks = new ArrayList<>();

    private String activation;

    private Boolean enabled = false;

    public User() {

    }

    public User(String email) {
        this.userName = email;
        this.emailAddress = email;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isGlobalAdmin() {
        return isGlobalAdmin;
    }

    public void setGlobalAdmin(boolean isGlobalAdmin) {
        this.isGlobalAdmin = isGlobalAdmin;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<User> getParents() {
        return parents;
    }

    public void setParents(List<User> parents) {
        this.parents = parents;
    }

    public List<User> getChildren() {
        return children;
    }

    public void setChildren(List<User> children) {
        this.children = children;
    }

    public List<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public StudentClass getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(StudentClass studentClass) {
        this.studentClass = studentClass;
    }

    public List<Lesson> getTeachersLessons() {
        return teachersLessons;
    }

    public void setTeachersLessons(List<Lesson> teachersLessons) {
        this.teachersLessons = teachersLessons;
    }

    public List<Lesson> getStudentsLessons() {
        return studentsLessons;
    }

    public void setStudentsLessons(List<Lesson> studentsLessons) {
        this.studentsLessons = studentsLessons;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addRole(Role role) {
        roles.add(role);
        role.setUser(this);
    }

    public void removeRole(Role role) {
        roles.remove(role);
    }
    
     public void clearRole() {
        roles.clear();
    }

    public void addTeachersLesson(Lesson lesson) {
        teachersLessons.add(lesson);
        lesson.setTeacher(this);
    }

    public void removeTeachersLesson(Lesson lesson) {
        teachersLessons.remove(lesson);
        lesson.setTeacher(null);
    }

    public void addParent(User user) {
        parents.add(user);
    }

    public void removeParent(User user) {
        parents.remove(user);
    }

    public void addChild(User user) {
        children.add(user);
    }

    public void removeChild(User user) {
        children.remove(user);
    }

    public void addQualification(Qualification qualification) {
        qualifications.add(qualification);
    }

    public void removeQualification(Qualification qualification) {
        qualifications.remove(qualification);
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }

    public void removeSubject(Subject subject) {
        subjects.remove(subject);
    }

    public void addStudentsLesson(Lesson lesson) {
        studentsLessons.add(lesson);
    }

    public void removeStudentsLesson(Lesson lesson) {
        studentsLessons.remove(lesson);
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    public void addMark(Mark mark) {
        marks.add(mark);
        mark.setUser(this);
    }

    public void removeMark(Mark mark) {
        marks.remove(mark);
        mark.setUser(null);
    }
}
