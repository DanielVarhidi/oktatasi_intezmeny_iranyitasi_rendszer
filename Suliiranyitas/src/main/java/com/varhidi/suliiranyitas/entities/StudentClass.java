package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**Tanulókból álló osztályok.
 *
 * @author Kovács Dániel
 */

@Entity
@Table(name="studentclass")
public class StudentClass extends BaseEntity implements Serializable {
    
    private String name;
    
    @ManyToOne
    private Institution institution;
    
    private int startYear;
    
    private int budget;
    
    @OneToMany(mappedBy = "studentClass", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<User> students = new ArrayList<>();
    
    @OneToMany(mappedBy = "studentClass", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Message> messageBoard = new ArrayList<>();
    
    //már nem lesz csoportbontás
    @OneToMany(mappedBy = "studentClass", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Lesson> lessons = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public List<User> getStudents() {
        return students;
    }

    public void setStudents(List<User> students) {
        this.students = students;
    }

    public void addStudent(User user){
        students.add(user);
        user.setStudentClass(this);
    }
    
    public void removeStudents(User user){
        students.remove(user);
        user.setStudentClass(null);
    }
    
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public List<Message> getMessageBoard() {
        return messageBoard;
    }

    public void setMessageBoard(List<Message> messageBoard) {
        this.messageBoard = messageBoard;
    }
    
    public void addMessage(Message message){
        messageBoard.add(message);
        message.setStudentClass(this);
    }
    
    public void removeMessage(Message message){
        messageBoard.remove(message);
        message.setStudentClass(null);
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }
    
    public void addLesson(Lesson lesson){
        lessons.add(lesson);
        lesson.setStudentClass(this);
    }
    
    public void removeLesson(Lesson lesson){
        lessons.remove(lesson);
        lesson.setStudentClass(null);
    }
}
