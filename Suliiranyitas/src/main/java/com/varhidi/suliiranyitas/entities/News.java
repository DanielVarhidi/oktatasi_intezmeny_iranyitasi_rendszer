package com.varhidi.suliiranyitas.entities;
import javax.persistence.Entity;

import java.io.Serializable;
import javax.persistence.ManyToOne;

/**Az intézmények hírei.
 *
 * @author Kovács Dániel
 */
@Entity
public class News extends BaseEntity implements Serializable {
    
    private String title;
    
    private String text;
    
    @ManyToOne
    private Institution institution;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String article) {
        this.text = article;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }
    
    
}
