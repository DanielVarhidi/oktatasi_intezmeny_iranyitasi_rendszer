package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * Intézmény.
 *
 * @author Kovács Dániel
 */
@Entity
public class Institution extends BaseEntity implements Serializable {

    private String name;

    private String address;

    private String taxNumber;

    private String contact;
    
    private double posLatitude;
    
    private double posLongitude;

    @OneToMany(mappedBy = "institute", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Role> members = new ArrayList<>();

    @OneToMany(mappedBy = "institution", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Lesson> lessons = new ArrayList<>();

    @OneToMany(mappedBy = "institution", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<StudentClass> studentClasses = new ArrayList<>();

    @OneToMany(mappedBy = "institution", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<News> newsBoard = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public List<Role> getMembers() {
        return members;
    }

    public void setMembers(List<Role> members) {
        this.members = members;
    }

    public void addMemberWithRole(Role role) {
        members.add(role);
        role.setInstitute(this);
    }

    public void removeMemberWithRole(Role role) {
        members.remove(role);
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public List<StudentClass> getStudentClasses() {
        return studentClasses;
    }

    public void setStudentClasses(List<StudentClass> studentClasses) {
        this.studentClasses = studentClasses;
    }

    public void addLesson(Lesson lesson) {
        lessons.add(lesson);
        lesson.setInstitution(this);
    }

    public void removeLesson(Lesson lesson) {
        lessons.remove(lesson);
        lesson.setInstitution(null);
    }

    public void addStudentClass(StudentClass sc) {
        studentClasses.add(sc);
        sc.setInstitution(this);
    }

    public void removeStudentClass(StudentClass sc) {
        studentClasses.remove(sc);
        sc.setInstitution(null);
    }

    public List<News> getNewsBoard() {
        return newsBoard;
    }

    public void setNewsBoard(List<News> newsBoard) {
        this.newsBoard = newsBoard;
    }

    public void addNews(News news) {
        newsBoard.add(news);
        news.setInstitution(this);
    }

    public void removeNews(News news) {
        newsBoard.remove(news);
        news.setInstitution(null);
    }
    
     public double getLatitude() {
        return posLatitude;
    }

    public void setLatitude(double Latitude) {
        this.posLatitude = Latitude;
    }

    public double getLongitude() {
        return posLongitude;
    }

    public void setLongitude(double Longitude) {
        this.posLongitude = Longitude;
    }
}
