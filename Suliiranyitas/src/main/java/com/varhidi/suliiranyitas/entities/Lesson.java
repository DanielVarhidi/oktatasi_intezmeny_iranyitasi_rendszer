package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**Tanórák, amikre a diákok járnak, és amit egy tanár tanít egy intézményben,
 *
 * @author User
 */
@Entity
public class Lesson extends BaseEntity implements Serializable{
    
    private String name;
    
    private String location;
    
    @ManyToOne
    private Institution institution;
    
    @ManyToOne
    private User teacher;
    
    //nem létezö csoportbontás miatt
    @ManyToMany(mappedBy = "studentsLessons", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<User> students = new ArrayList<>();
    
    //az osztály, mert nem lesz csoportbontás
    @ManyToOne
    private StudentClass studentClass;
    
    //erre a kettöre már nincs szükség, majd kitörlöm
    //kezdés ideje órában -> LessonItem
    private int time;
    
    //melyik napon -> LessonItem
    @Enumerated(EnumType.STRING)
    private Day day;

    //az egyes tanórák
    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
    private List<LessonItem> lessonItems = new ArrayList<>();
    
    //óraszám
    private int lessonsPerWeek;
    
    //osztályzat
    @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
    private List<Mark> marks = new ArrayList<>();
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public List<User> getStudents() {
        return students;
    }

    public void setStudents(List<User> students) {
        this.students = students;
    }

    public void addStudent(User user){
        students.add(user);
    }
    
    public void removeStudent(User user){
        students.remove(user);
    }
    
    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institute) {
        this.institution = institute;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }
 
    public void addMark(Mark mark){
        marks.add(mark);
        mark.setLesson(this);
    }
    
    public void removeMark(Mark mark){
        marks.remove(mark);
        mark.setLesson(null);
    }

    public List<LessonItem> getLessonItems() {
        return lessonItems;
    }

    public void setLessonItems(List<LessonItem> lessonItems) {
        this.lessonItems = lessonItems;
    }
    
    public void addLessonItem(LessonItem lessonItem){
        lessonItems.add(lessonItem);
        lessonItem.setLesson(this);
    }
    
    public void removeLessonItem(LessonItem lessonItem){
        lessonItems.remove(lessonItem);
        lessonItem.setLesson(null);
    }

    public int getLessonsPerWeek() {
        return lessonsPerWeek;
    }

    public void setLessonsPerWeek(int lessonsPerWeek) {
        this.lessonsPerWeek = lessonsPerWeek;
    }

    public StudentClass getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(StudentClass studentClass) {
        this.studentClass = studentClass;
    }
    
}
