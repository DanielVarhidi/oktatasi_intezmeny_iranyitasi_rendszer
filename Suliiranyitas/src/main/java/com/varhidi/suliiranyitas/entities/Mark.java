package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**Osztályzat.
 *
 * @author User
 */
@Entity
public class Mark extends BaseEntity implements Serializable{
    
    @ManyToOne
    private User user;
    
    @ManyToOne
    private Lesson lesson;
    
    //1-5
    @Min(1)
    @Max(5)
    private int mark;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
    
    
}
