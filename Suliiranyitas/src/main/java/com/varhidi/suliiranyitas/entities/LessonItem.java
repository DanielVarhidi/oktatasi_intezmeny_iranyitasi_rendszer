package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

/**Tanórák.
 *
 * @author Kovács Dani
 */
@Entity
public class LessonItem extends BaseEntity implements Serializable {
    
    private int hour;
    
    @Enumerated(EnumType.STRING)
    private Day day;
    
    @ManyToOne
    private Lesson lesson;
    
    

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }
    
    
}
