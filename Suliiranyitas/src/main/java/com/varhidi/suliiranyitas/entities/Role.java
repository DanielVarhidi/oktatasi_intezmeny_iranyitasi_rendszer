package com.varhidi.suliiranyitas.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**A felhsználókat köti össze az intézményekkel, ezzel együtt megadja, hogy
 * ott milyen szerepkörrel rendelkezik.
 *
 * @author Kovács Dániel
 */

@Entity
@Table(name="schoolrole")
public class Role extends BaseEntity implements Serializable {
    
    @ManyToOne
    private User user;
    
    @ManyToOne
    private Institution institute;
    
    //@ManyToOne
    //Institution contact;
    
    @Enumerated(EnumType.STRING)
    private UserRole role;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Institution getInstitute() {
        return institute;
    }

    public void setInstitute(Institution institute) {
        this.institute = institute;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.user);
        hash = 37 * hash + Objects.hashCode(this.institute);
        hash = 37 * hash + Objects.hashCode(this.role);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Role other = (Role) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.institute, other.institute)) {
            return false;
        }
        if (!this.role.equals(other.role)) {
            return false;
        }
        return true;
    }
    
    
}
