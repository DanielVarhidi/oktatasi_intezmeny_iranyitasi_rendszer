package com.varhidi.suliiranyitas.entities;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/*
sql script a létrehozáshoz és törléshez
DROP DATABASE `suliiranyitas`;
CREATE SCHEMA `suliiranyitas` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci ;
*/

/**Alaposztály az entitásokhoz.
 * 
 * @author Kovács Dániel
 */

@MappedSuperclass
public class BaseEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    Date lastUpdatedAt;

    @PrePersist
    public void prePersist() {
        createdAt = new Date();
        lastUpdatedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        lastUpdatedAt = new Date();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Date lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }
    
    
}
