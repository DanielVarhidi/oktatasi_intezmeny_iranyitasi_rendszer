package com.varhidi.suliiranyitas.entities;

/**
 *
 * @author Kovács Dániel
 */
public enum Gender {
    MALE,
    FEMALE;
}
