package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.dtos.RoleDto;
import com.varhidi.suliiranyitas.entities.Role;
import com.varhidi.suliiranyitas.entities.UserRole;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class MainService {

    @Autowired
    UserService us;
    
    @PersistenceContext
    EntityManager em;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MainService.class);
    
    public List<RoleDto> getRoles(String userName){
        List result = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.RoleDto(r.id, r.institute.name, r.role) FROM User u JOIN u.roles r WHERE u.userName = :usern")
            .setParameter("usern", userName)
            .getResultList();
        return result;
    }
    
    public void changeRole(Long id){
        Role role = em.find(Role.class, id);
        if(role == null){
            LOGGER.info( "invalid role id during role change" );
            return;
        }
        UserRole userRole = role.getRole();
        LOGGER.info( "userRole is changing to " + userRole.name() );
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        OurUserDetails userDetails = (OurUserDetails)auth.getPrincipal();
        
        if( !userDetails.getUser().getId().equals(role.getUser().getId()) ){
            LOGGER.info( "unauthorized role change" );
            return;
        }
        
        userDetails.setInstitution(role.getInstitute());
        userDetails.setCurrentAuth(new SimpleGrantedAuthority(userRole.name()));
        List<GrantedAuthority> updatedAuthorities = new ArrayList<>();
        updatedAuthorities.add( new SimpleGrantedAuthority(userRole.name()) );     //add your role here [e.g., new SimpleGrantedAuthority("ROLE_NEW_ROLE")]
        Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(), updatedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(newAuth);
        
        LOGGER.info("new authority granted, old one lost");
    }
}
