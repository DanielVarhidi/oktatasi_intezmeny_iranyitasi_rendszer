/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.dtos.StudentResultDto;
import com.varhidi.suliiranyitas.entities.Lesson;
import com.varhidi.suliiranyitas.entities.Mark;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.service.autoDao.LessonAutoDao;
import com.varhidi.suliiranyitas.service.autoDao.MarkAutoDao;
import com.varhidi.suliiranyitas.service.autoDao.UserAutoDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Áron
 */
@Component
public class StudentResultService {

    @Autowired
    MarkAutoDao markAutoDao;

    @Autowired
    LessonAutoDao lessonAutoDao;

    @Autowired
    UserAutoDao userAutoDao;
    //TODO do this query with one join query
    public List<StudentResultDto> getResultsForStudentByLesson(User student) {

        List<StudentResultDto> result = new ArrayList<>();

        List<Lesson> lessonsForUser = lessonAutoDao.findByStudents_UserName(student.getUserName());

        for (Lesson l : lessonsForUser) {

            StudentResultDto s = new StudentResultDto();
            s.setNameOfLesson(l.getName());
            s.setStudentName(student.getName());

            List<Mark> marksForStudentAndLesson = markAutoDao.findByUserAndLesson(student, l);
            List<Integer> marksAsInts = new ArrayList<>();
            for (Mark m : marksForStudentAndLesson) {
                marksAsInts.add(m.getMark());
            }

            s.setMarks(marksAsInts);

            result.add(s);
        }

        return result;
    }

}
