/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.config.WebSecurityConfig;
import com.varhidi.suliiranyitas.dtos.UserDto;
import com.varhidi.suliiranyitas.emailsender.EmailSender;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.Role;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.entities.UserRole;
import com.varhidi.suliiranyitas.service.autoDao.UserAutoDao;
import java.util.Random;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author István
 */
@Service
public class UserService {

    private EmailSender emailsender;

    @Autowired
    public void setEmailSender(EmailSender emailsender) {
        this.emailsender = emailsender;
    }

    @Autowired
    UserAutoDao userRepository;

    @Autowired
    private DozerBeanMapper beanMapper;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void save(User user) {
        userRepository.save(user);
        em.refresh(this);
    }

    @Transactional
    public void updateUser(User user) {
        em.refresh(user);
    }

    @Transactional
    public void update(UserDto userDto) {
        User user = em.find(User.class, userDto.getId());
        beanMapper.map(userDto, user);
    }

    public User getCurrentUser() {
        OurUserDetails currentUserDetails = getCurrentUserDetails();
        return currentUserDetails == null ? null : currentUserDetails.getUser();
    }

    public OurUserDetails getCurrentUserDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if(principal instanceof String){
            return null;
        }
        OurUserDetails userDetails = (OurUserDetails) auth.getPrincipal();
        return userDetails;
    }

    public UserDto getCurrentUserDto() {
        OurUserDetails oud = (OurUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = oud.getUser();
        UserDto userDto = new UserDto();
        beanMapper.map(user, userDto);
        return userDto;
    }

    public User getUserByUserName(String userName) throws UsernameNotFoundException {
        Query query = em.createQuery("SELECT un FROM User un WHERE un.userName=:usrn ");
        query.setParameter("usrn", userName);
        User u = (User) query.getSingleResult();
        return u;
    }

    public Role getCurrentRole() {
        OurUserDetails oud = (OurUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = oud.getUser();
        return user.getRoles().get(0);
    }

    public String generateKey() {
        Random random = new Random();
        char[] word = new char[32];
        for (int j = 0; j < word.length; j++) {
            word[j] = (char) ('a' + random.nextInt(26));
        }
        return new String(word);
    }

    @Transactional
    public void savePreRegisteredUser(String email, String random, UserRole userRole, Institution institution, String randomPassword) {
        String activation = userRole + random;

        User user = new User(email);
        user.setActivation(activation);
        Role role = new Role();
        role.setInstitute(institution);
        role.setUser(user);
        role.setRole(userRole);
        user.addRole(role);
        user.setPassword(WebSecurityConfig.passwordEncoder().encode(randomPassword));
        userRepository.save(user);
        em.persist(role);

    }

    @Transactional
    public void saveTeacher(Institution institution, String email) throws MessagingException {
        String randomPassword = generateKey();
        String random = generateKey();
        emailsender.sendInstAdminRegMessage(email, random, randomPassword);
        savePreRegisteredUser(email, random, UserRole.TEMP_TEACHER, institution, randomPassword);
    }

    @Transactional
    public void saveParent(Institution institution, String email) throws MessagingException {
        String randomPassword = generateKey();
        String random = generateKey();
        emailsender.sendInstAdminRegMessage(email, random, randomPassword);
        savePreRegisteredUser(email, random, UserRole.TEMP_PARENT, institution, randomPassword);
    }

    @Transactional
    public void saveStudent(Institution institution, String email) throws MessagingException {
        String randomPassword = generateKey();
        String random = generateKey();
        emailsender.sendInstAdminRegMessage(email, random, randomPassword);
        savePreRegisteredUser(email, random, UserRole.TEMP_STUDENT, institution, randomPassword);
    }

    @Transactional
    public void saveLocalAdmin(Institution institution, String email) throws MessagingException {
        String randomPassword = generateKey();
        String random = generateKey();
        emailsender.sendInstAdminRegMessage(email, random, randomPassword);
        savePreRegisteredUser(email, random, UserRole.TEMP_ADMINISTRATOR, institution, randomPassword);
    }
}
