/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.dtos.ClassDto;
import com.varhidi.suliiranyitas.dtos.InstitutionDto;
import com.varhidi.suliiranyitas.dtos.MessageDto;
import com.varhidi.suliiranyitas.entities.Message;
import com.varhidi.suliiranyitas.entities.StudentClass;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.service.autoDao.LessonAutoDao;
import com.varhidi.suliiranyitas.service.autoDao.MessageAutoDao;
import com.varhidi.suliiranyitas.service.autoDao.UserAutoDao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Áron
 */
@Component
public class MessageService {

    @Autowired
    MessageAutoDao messageAutoDao;

    @Autowired
    LessonAutoDao lessonAutoDao;

    @Autowired
    UserService userService;

    @Autowired
    UserAutoDao userAutoDao;

    @PersistenceContext
    EntityManager em;

    @Autowired
    private DozerBeanMapper beanMapper;

    public List<MessageDto> findMessagesOfClass() {

        User currentUser = userService.getCurrentUser();
        return findMessagesByClass(currentUser.getStudentClass().getId());
    }

    public List<MessageDto> findMessagesByClass(Long id) {
        List<Message> allMessageOfClass = messageAutoDao.findByStudentClassId(id);
        List<MessageDto> allMessageDtoOfClass = new ArrayList<>();
        for (Message message : allMessageOfClass) {
            MessageDto messageDto = new MessageDto();
            messageDto.setAuthor(message.getAuthor());
            messageDto.setText(message.getText());
            allMessageDtoOfClass.add(messageDto);
        }
        return allMessageDtoOfClass;
    }

    @Transactional
    public void addNewMessage(MessageDto messageDto) {
        Message message = new Message();
        message.setAuthor(userService.getCurrentUser().getName());
        message.setText(messageDto.getText());

        message.setStudentClass(new StudentClass());
        if (userService.getCurrentUserDetails().isStudent()) {
            message.setStudentClass(userService.getCurrentUser().getStudentClass());
        } else {
            message.getStudentClass().setId(messageDto.getStudentClassId());
        }
        em.persist(message);
    }

    public List<ClassDto> findAllClassOfEachTeacher() {
        User currentUser = userService.getCurrentUser();

        List<StudentClass> allClassOfEachTeacher
                = em.createQuery("SELECT DISTINCT cl from Lesson le JOIN le.studentClass cl where le.teacher.id = :teacherId")
                        .setParameter("teacherId", currentUser.getId())
                        .getResultList();
        List<ClassDto> allClassDtoOfTeacher = new ArrayList<>();

        for (StudentClass studentClass : allClassOfEachTeacher) {
            ClassDto classDto = new ClassDto();
            classDto.setId(studentClass.getId());
            classDto.setName(studentClass.getName());
            classDto.setStartYear(studentClass.getStartYear());
            InstitutionDto institutionDto = beanMapper.map(studentClass.getInstitution(), InstitutionDto.class, "institutionMapWithoutMembers");
            classDto.setInstitution(institutionDto);
            allClassDtoOfTeacher.add(classDto);
        }
        return allClassDtoOfTeacher;
    }

}
