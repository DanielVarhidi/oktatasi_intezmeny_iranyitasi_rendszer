/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.dtos.AddMarkStudentDto;
import com.varhidi.suliiranyitas.dtos.LessonDto;
import com.varhidi.suliiranyitas.entities.Lesson;
import com.varhidi.suliiranyitas.entities.Mark;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.service.autoDao.LessonAutoDao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Áron
 */
@Component
public class MarkRegistrateService {

    @Autowired
    LessonAutoDao lessonAutoDao;

    @Autowired
    private DozerBeanMapper beanMapper;

    @PersistenceContext
    EntityManager em;

    public List<LessonDto> getAllLessonsOfTeacher(User actualTeacher) {
        List<Lesson> allLessonsOfTeacher = lessonAutoDao.findByTeacher_UserName(actualTeacher.getUserName());
        List<LessonDto> allLessonDtosOfTeacher = new ArrayList<>();
        allLessonsOfTeacher.forEach((Lesson lesson) -> {
            allLessonDtosOfTeacher.add(beanMapper.map(lesson, LessonDto.class, "lessonsOfTeacher"));
        });
        return allLessonDtosOfTeacher;
    }

    public List<AddMarkStudentDto> getAllStudentsOfLesson(Long lessonId) {
        List<User> students = em.createQuery("select u from User u JOIN u.studentsLessons s where s.id = :id")
                .setParameter("id", lessonId)
                .getResultList();
        List<AddMarkStudentDto> studentsDto = new ArrayList<>();

        for (User student : students) {
            AddMarkStudentDto studentDto = new AddMarkStudentDto();
            studentDto.setStudentId(student.getId());
            studentDto.setStudentName(student.getName());
            studentsDto.add(studentDto);
        }

        return studentsDto;
    }

    @Transactional
    public void persistNewMark(Long studentId, Long lessonId, int markNum) {
        User student = em.find(User.class, studentId);
        Lesson lesson = em.find(Lesson.class, lessonId);
        Mark mark = new Mark();
        mark.setUser(student);
        mark.setLesson(lesson);
        mark.setMark(markNum);
        em.persist(mark);
    }
}
