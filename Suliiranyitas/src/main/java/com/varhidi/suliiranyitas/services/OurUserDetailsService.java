package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.entities.User;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Eszter
 */
@Service
public class OurUserDetailsService implements UserDetailsService {

    @PersistenceContext
    EntityManager em;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Query query = em.createQuery("SELECT un FROM User un WHERE un.userName=:usrn ");
        query.setParameter("usrn", userName);
        EntityGraph<?> eg = em.createEntityGraph("userWithRolesAndClass");
        query.setHint("javax.persistence.loadgraph", eg);
        User u = (User) query.getSingleResult();
        u.getRoles().size();
        OurUserDetails userDetails = new OurUserDetails(u);
        return userDetails;
    }

}
