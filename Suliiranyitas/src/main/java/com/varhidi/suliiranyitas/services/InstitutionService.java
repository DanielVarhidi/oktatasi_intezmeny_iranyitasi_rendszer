package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.dtos.InstitutionDto;
import com.varhidi.suliiranyitas.emailsender.EmailSender;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.UserRole;
import com.varhidi.suliiranyitas.service.autoDao.InstitutionAutoDao;
import java.util.ArrayList;
import java.util.List;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.dozer.DozerBeanMapper;

/**
 * @author DaniKiss
 */

@Component
public class InstitutionService {
    
        
    private EmailSender emailsender;
    
    @Autowired
    UserService userService;
    
    @Autowired
    public void setEmailSender(EmailSender emailsender){
        this.emailsender=emailsender;
    }
    
    @PersistenceContext
    EntityManager entityManager;
    
    @Autowired
    InstitutionAutoDao institutionAutoDao;
    
    @Autowired
    private DozerBeanMapper beanMapper;
    
    @Transactional
    public List<InstitutionDto> listAllInstitution() {
        
        List<Institution> allInstitutions = institutionAutoDao.findAll();
        List<InstitutionDto> allInstitutionDtos = new ArrayList<>();
        allInstitutions.forEach((institution) -> {
            allInstitutionDtos.add(beanMapper.map(institution, InstitutionDto.class,"institutionMapWithoutMembers"));
        });
        return allInstitutionDtos;
    }
    
    public InstitutionDto findInstitutionById(Long id){
        Institution temp= institutionAutoDao.getOne(id);
        InstitutionDto searched=new InstitutionDto();
        searched.setAddress(temp.getAddress());
        searched.setContact(temp.getContact());
        searched.setId(temp.getId());
        searched.setName(temp.getName());
        searched.setPosLatitude(temp.getLatitude());
        searched.setPosLongitude(temp.getLongitude());
        searched.setTaxNumber(temp.getTaxNumber());
        
        return searched;
    }
    
    @Transactional
    public InstitutionDto newInstitutionDto(){
     InstitutionDto institution= new InstitutionDto();
     return institution;
    }
    
    @Transactional
    public void addInstitutionIntoTheDataBase(InstitutionDto institutionDto) throws MessagingException{
        Institution institution= new Institution();
        institution.setName(institutionDto.getName());
        institution.setAddress(institutionDto.getAddress());
        institution.setTaxNumber(institutionDto.getTaxNumber());
        institution.setContact(institutionDto.getContact());
        institution.setLatitude(institutionDto.getPosLatitude());
        institution.setLongitude(institutionDto.getPosLongitude());
        
        String randomPassword=userService.generateKey();
        String random=userService.generateKey();
        String email=institutionDto.getLocalAdminEmailAdress();
        emailsender.sendInstAdminRegMessage(email, random,randomPassword);
        UserRole userRole=UserRole.TEMP_ADMINISTRATOR;
        
        userService.savePreRegisteredUser(email, random, userRole, institution, randomPassword);
        
        entityManager.persist(institution);
    }
    
    
    
}
