package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.entities.Day;
import com.varhidi.suliiranyitas.entities.Gender;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.Lesson;
import com.varhidi.suliiranyitas.entities.LessonItem;
import com.varhidi.suliiranyitas.entities.Mark;
import com.varhidi.suliiranyitas.entities.Message;
import com.varhidi.suliiranyitas.entities.News;
import com.varhidi.suliiranyitas.entities.Qualification;
import com.varhidi.suliiranyitas.entities.Role;
import com.varhidi.suliiranyitas.entities.StudentClass;
import com.varhidi.suliiranyitas.entities.Subject;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.entities.UserRole;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
//import org.springframework.transaction.annotation.Transactional;

/**
 * Tesztadatokat töltünk be a program indításakor.
 *
 * @author Kovács Dani
 */
@Component
public class DataLoader implements ApplicationRunner {

    @PersistenceContext
    EntityManager em;

    @Autowired
    PasswordEncoder passwordEncoder;

    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);
    
    @Value("${suliiranyitas.usernr}")
    private Integer nrOfUsers;

    @Value("${suliiranyitas.institutionnr}")
    private Integer nrOfInstitutions;

    private static final List<String> lastNames = new ArrayList<>();
    private static final List<String> maleFirstNames = new ArrayList<>();
    private static final List<String> femaleFirstNames = new ArrayList<>();
    private static final List<String> cities = new ArrayList<>();
    private static final List<String> emails = new ArrayList<>();
    private static final List<String> instituteNames = new ArrayList<>();
    private static final List<String> qualifiactionNames = new ArrayList<>();
    private static final Map<String, Double> latitudes = new HashMap<>();
    private static final Map<String, Double> longitudes = new HashMap<>();

    private static List<User> users = new ArrayList<>();
    private static List<User> students = new ArrayList<>();
    private static List<User> teachers = new ArrayList<>();
    private static List<User> adults = new ArrayList<>();
    private static List<Role> roles = new ArrayList<>();
    private static List<Institution> institutions = new ArrayList<>();
    private static List<Qualification> qualifications = new ArrayList<>();
    private static List<Subject> subjects = new ArrayList<>();
    private static List<StudentClass> studentClasses = new ArrayList<>();
    private static List<Lesson> lessons = new ArrayList<>();

    //private static int userNr = 0;
    static {
        lastNames.add("Kovács");
        lastNames.add("Szabó");
        lastNames.add("Tóth");
        lastNames.add("Horváth");
        lastNames.add("Nagy");
        lastNames.add("Kiss");
        lastNames.add("Papp");
        lastNames.add("Pék");
        lastNames.add("Mészáros");
        lastNames.add("Katona");
        lastNames.add("Hentes");
        lastNames.add("Lakatos");
        lastNames.add("Molnár");
        lastNames.add("Széles");
        lastNames.add("Vékony");
        lastNames.add("Szende");
        lastNames.add("Morgó");
        lastNames.add("Fekete");
        lastNames.add("Piros");
        lastNames.add("Vörös");
        lastNames.add("Sárga");
        lastNames.add("Kék");
        lastNames.add("Zöld");
        lastNames.add("Fehér");
        lastNames.add("Pesti");
        lastNames.add("Budai");
        lastNames.add("Szegedi");
        lastNames.add("Debreceni");
        lastNames.add("Zalai");
        lastNames.add("Gyulai");
        lastNames.add("Bécsi");
        lastNames.add("Pozsonyi");
        lastNames.add("Somogyi");
        lastNames.add("Esztergomi");
        lastNames.add("Szolnoki");
        lastNames.add("Csongrádi");
        lastNames.add("Bükki");
        lastNames.add("Vas");
        lastNames.add("Német");
        lastNames.add("Szerb");
        lastNames.add("Lengyel");
        lastNames.add("Orosz");
        lastNames.add("Francia");
        lastNames.add("Spanyol");
        lastNames.add("Belga");
        lastNames.add("Víz");
        lastNames.add("Föld");
        lastNames.add("Hold");
        lastNames.add("Merkúr");
        lastNames.add("Vénusz");
        lastNames.add("Mars");
        lastNames.add("Jupiter");
        lastNames.add("Szaturnusz");
        lastNames.add("Uránusz");
        lastNames.add("Neptunusz");
        lastNames.add("Plútó");
        lastNames.add("Nap");
        lastNames.add("Példa");
        lastNames.add("Másik");
        lastNames.add("Egyik");
        lastNames.add("Kósza");
        lastNames.add("Kockás");
        lastNames.add("Csíkos");
        lastNames.add("Foltos");
        lastNames.add("Fakó");
        lastNames.add("Színes");
        lastNames.add("Keresztes");
        lastNames.add("Tavaszi");
        lastNames.add("Nyári");
        lastNames.add("Öszi");
        lastNames.add("Téli");
        lastNames.add("Január");
        lastNames.add("Február");
        lastNames.add("Március");
        lastNames.add("Április");
        lastNames.add("Május");
        lastNames.add("Június");
        lastNames.add("Július");
        lastNames.add("Augusztus");
        lastNames.add("Szeptember");
        lastNames.add("Október");
        lastNames.add("November");
        lastNames.add("December");
        lastNames.add("Alfa");
        lastNames.add("Béta");
        lastNames.add("Gamma");
        lastNames.add("Delta");
        lastNames.add("Epszilon");
        lastNames.add("Éta");
        lastNames.add("Zeta");
        lastNames.add("Theta");
        lastNames.add("Ióta");
        lastNames.add("Kappa");
        lastNames.add("Lambda");
        lastNames.add("Mü");
        lastNames.add("Nü");
        lastNames.add("Omikron");
        lastNames.add("Pí");
        lastNames.add("Rhó");
        lastNames.add("Szigma");
        lastNames.add("Tau");
        lastNames.add("Üpszilon");
        lastNames.add("Fí");
        lastNames.add("Khí");
        lastNames.add("Pszí");
        lastNames.add("Omega");

        maleFirstNames.add("Péter");
        maleFirstNames.add("Pál");
        maleFirstNames.add("Ferenc");
        maleFirstNames.add("Gergely");
        maleFirstNames.add("János");
        maleFirstNames.add("Aladár");
        maleFirstNames.add("Béla");
        maleFirstNames.add("Dénes");
        maleFirstNames.add("Elemér");
        maleFirstNames.add("Géza");
        maleFirstNames.add("Huba");
        maleFirstNames.add("István");
        maleFirstNames.add("Kálmán");
        maleFirstNames.add("László");
        maleFirstNames.add("Miklós");
        maleFirstNames.add("Norbert");
        maleFirstNames.add("Ottó");
        maleFirstNames.add("Róbert");
        maleFirstNames.add("Sándor");
        maleFirstNames.add("Tamás");
        maleFirstNames.add("Ubul");
        maleFirstNames.add("Viktor");
        maleFirstNames.add("Zoltán");

        femaleFirstNames.add("Anna");
        femaleFirstNames.add("Bernadett");
        femaleFirstNames.add("Cecil");
        femaleFirstNames.add("Diána");
        femaleFirstNames.add("Eszter");
        femaleFirstNames.add("Fanni");
        femaleFirstNames.add("Gertrúd");
        femaleFirstNames.add("Hajnalka");
        femaleFirstNames.add("Ibolya");
        femaleFirstNames.add("Júlia");
        femaleFirstNames.add("Kitti");
        femaleFirstNames.add("Linda");
        femaleFirstNames.add("Mária");
        femaleFirstNames.add("Nikolett");
        femaleFirstNames.add("Orsolya");
        femaleFirstNames.add("Panka");
        femaleFirstNames.add("Rita");
        femaleFirstNames.add("Sára");
        femaleFirstNames.add("Tímea");
        femaleFirstNames.add("Ursula");
        femaleFirstNames.add("Viktória");

        cities.add("Budapest");
        cities.add("Szeged");
        cities.add("Debrecen");
        cities.add("Nyíregyháza");
        cities.add("Zalaegerszeg");
        cities.add("Békéscsaba");
        cities.add("Tatabánya");
        cities.add("Komárom");
        cities.add("Pápa");
        cities.add("Sopron");
        cities.add("Pécs");
        cities.add("Miskolc");

        latitudes.put("Budapest", 47.49883);
        longitudes.put("Budapest", 19.04083);
        latitudes.put("Szeged", 46.25);
        longitudes.put("Szeged", 20.16666);
        latitudes.put("Debrecen", 47.53);
        longitudes.put("Debrecen", 21.63916);
        latitudes.put("Nyíregyháza", 47.95306);
        longitudes.put("Nyíregyháza", 21.72713);
        latitudes.put("Zalaegerszeg", 46.84538);
        longitudes.put("Zalaegerszeg", 16.84721);
        latitudes.put("Békéscsaba", 46.67889);
        longitudes.put("Békéscsaba", 21.09083);
        latitudes.put("Tatabánya", 47.58161);
        longitudes.put("Tatabánya", 18.39485);
        latitudes.put("Komárom", 47.743);
        longitudes.put("Komárom", 18.119);
        latitudes.put("Pápa", 47.33055);
        longitudes.put("Pápa", 17.46583);
        latitudes.put("Sopron", 47.68489);
        longitudes.put("Sopron", 16.58305);
        latitudes.put("Pécs", 46.07083);
        longitudes.put("Pécs", 18.23305);
        latitudes.put("Miskolc", 48.10416);
        longitudes.put("Miskolc", 20.79139);
        
        emails.add("@gugl.hu");
        emails.add("@gugl.com");
        emails.add("@frimail.hu");
        emails.add("@frimail.com");
        emails.add("@hotmal.hu");
        emails.add("@hotmal.com");
        emails.add("@jahu.hu");
        emails.add("@jahu.com");
        emails.add("@example.hu");
        emails.add("@example.com");

        instituteNames.add("Általános Iskola");
        instituteNames.add("Szakközépiskola");
        instituteNames.add("Gimnázium");
        instituteNames.add("Főiskola");
        instituteNames.add("Egyetem");

        qualifiactionNames.add("magyar");
        qualifiactionNames.add("angol");
        qualifiactionNames.add("német");
        qualifiactionNames.add("török");
        qualifiactionNames.add("mongol");
        qualifiactionNames.add("matematika");
        qualifiactionNames.add("fizika");
        qualifiactionNames.add("kémia");
        qualifiactionNames.add("történelem");
        qualifiactionNames.add("testnevelés");
        qualifiactionNames.add("környezetvédelem");
        qualifiactionNames.add("belső építész");
        qualifiactionNames.add("építőmérnök");
        qualifiactionNames.add("állatorvos");
    }

    @Override
    @Transactional
    public void run(ApplicationArguments args) {

        if (em.createQuery("SELECT u FROM User u").getResultList().size() != 0) {
            LOGGER.info("random data generation skipped");
            return;
        }
        LOGGER.info("random data generation started");
        
        User user1 = new User();
        User user2 = new User();
        User teacher = new User();
        Institution inst1 = new Institution();
        Institution inst2 = new Institution();
        Role role1 = new Role();
        Role role2 = new Role();
        Role teacherRole = new Role();

        User globalAdmin = new User();
        globalAdmin.setGlobalAdmin(true);
        globalAdmin.setName("Admin Ádám");
        globalAdmin.setUserName("admin");
        globalAdmin.setPassword(passwordEncoder.encode("admin"));
        globalAdmin.setEnabled(Boolean.TRUE);
        Role gaRole = new Role();
        gaRole.setRole(UserRole.GLOBAL_ADMINISTRATOR);
        globalAdmin.addRole(gaRole);
        em.persist(globalAdmin);

        User locAdmin = new User();
        Role roleLa = new Role();
        locAdmin.setName("locadmin");
        locAdmin.setUserName("locadmin");
        locAdmin.setPassword(passwordEncoder.encode("password"));
        locAdmin.addRole(roleLa);
        inst1.addMemberWithRole(roleLa);
        roleLa.setRole(UserRole.LOCAL_ADMINISTRATOR);
        locAdmin.setEnabled(Boolean.TRUE);
        em.persist(locAdmin);

        //User user1 = new User();
        user1.setAddress("Budapest");
        user1.setBirthDate(new Date());
        user1.setBirthPlace("Érd");
        user1.setEmailAddress("user1@example.hu");
        user1.setGender(Gender.MALE);
        user1.setMothersName("Példa Anyuka");
        user1.setName("Példa Béla");
        user1.setPassword(passwordEncoder.encode("password"));
        user1.setPersonalIdNumber("123456789");
        user1.setPhoneNumber("06303334444");
        user1.setUserName("peldabela");
//        user1.setChildren(children);
        List<User> parents = new ArrayList<>();
        parents.add(user2);
        user1.setParents(parents);
//        user1.setQualifications(qualifications);
        List<Role> roles = new ArrayList<>();
        roles.add(role1);
        user1.setRoles(roles);
//        user1.setStudentClass(studentClass);
//        user1.setStudentsLessons(studentsLessons);
//        user1.setSubjects(subjects);
//        user1.setTeachersLessons(teachersLessons);

        //User user2 = new User();
        user2.setAddress("Budapest");
        user2.setBirthDate(new Date());
        user2.setBirthPlace("Bicske");
        user2.setEmailAddress("user2@example.hu");
        user2.setGender(Gender.FEMALE);
        user2.setMothersName("Példa Anyuka Anyuka");
        user2.setName("Példa Anyuka");
        user2.setPassword(passwordEncoder.encode("password"));
        user2.setPersonalIdNumber("754634453");
        user2.setPhoneNumber("06303334445");
        user2.setUserName("peldaanyuka");

        teacher.setAddress("Budapest");
        teacher.setBirthDate(new Date());
        teacher.setBirthPlace("Bicske");
        teacher.setEmailAddress("teacher@example.hu");
        teacher.setGender(Gender.FEMALE);
        teacher.setMothersName("Példa Tanár Anyuka");
        teacher.setName("Példa Tanár");
        teacher.setPassword(passwordEncoder.encode("password"));
        teacher.setPersonalIdNumber("754634953");
        teacher.setPhoneNumber("06303334488");
        teacher.setUserName("tanar");

        List<User> children = new ArrayList<>();
        children.add(user1);
        user2.setChildren(children);
//        user2.setParents(parents);
//        user2.setQualifications(qualifications);
        roles.clear();
        roles.add(role2);
        user2.setRoles(roles);
//        user2.setStudentClass(studentClass);
//        user2.setStudentsLessons(studentsLessons);
//        user2.setSubjects(subjects);
//        user2.setTeachersLessons(teachersLessons);

        //Institution inst1 = new Institution();
        inst1.setAddress("Budapest");
        inst1.setName("Budapesti Iskola 1");
        inst1.setTaxNumber("12-23-34444");
//        inst1.setContact(user2);
        List<Role> members = new ArrayList<>();
        members.add(role1);
        members.add(role2);
        inst1.setMembers(members);

        //Institution inst2 = new Institution();
        inst2.setAddress("Budapest");
        inst2.setName("Budapesti Iskola 2");
        inst2.setTaxNumber("12-55-76541");
//        inst2.setContact(user2);
//        inst2.setMembers(members);

        //Role role1 = new Role();
        role1.setInstitute(inst1);
        role1.setRole(UserRole.STUDENT);
        role1.setUser(user1);

        //Role role2 = new Role();
        role2.setInstitute(inst1);
        role2.setRole(UserRole.PARENT);
        role2.setUser(user2);

        teacherRole.setRole(UserRole.TEACHER);
        inst1.addMemberWithRole(teacherRole);
        teacher.addRole(teacherRole);

        //pelda bela = user1
        StudentClass sc = new StudentClass();
        //sc.setInstitution(user1.getRoles().get(0).getInstitute());
        inst1.addStudentClass(sc);
        user1.setStudentClass(sc);

        Lesson lesson = new Lesson();
        lesson.setName("szorgalom");
        lesson.addStudent(user1);
        user1.addStudentsLesson(lesson);
        inst1.addLesson(lesson);
        teacher.addTeachersLesson(lesson);

        lesson = new Lesson();
        lesson.setName("magatartás");
        lesson.addStudent(user1);
        user1.addStudentsLesson(lesson);
        inst1.addLesson(lesson);
        teacher.addTeachersLesson(lesson);

        Mark mark = new Mark();
        lesson = user1.getStudentsLessons().get(0);
        user1.addMark(mark);
        lesson.addMark(mark);
        mark.setMark(4);

        mark = new Mark();
        lesson = user1.getStudentsLessons().get(0);
        user1.addMark(mark);
        lesson.addMark(mark);
        mark.setMark(5);

        mark = new Mark();
        lesson = user1.getStudentsLessons().get(1);
        user1.addMark(mark);
        lesson.addMark(mark);
        mark.setMark(3);

        //felhasználó több szereppel
        User multiUser = new User();
        Role multiRole1 = new Role();
        Role multiRole2 = new Role();
        Role multiRole3 = new Role();
        Role multiRole4 = new Role();

        multiUser.setUserName("multi");
        multiUser.setPassword(passwordEncoder.encode("password"));
        multiUser.setName("Multi User");

        multiUser.addRole(multiRole1);
        inst1.addMemberWithRole(multiRole1);
        multiRole1.setRole(UserRole.LOCAL_ADMINISTRATOR);

        multiUser.addRole(multiRole2);
        inst1.addMemberWithRole(multiRole2);
        multiRole2.setRole(UserRole.TEACHER);

        multiUser.addRole(multiRole3);
        inst2.addMemberWithRole(multiRole3);
        multiRole3.setRole(UserRole.LOCAL_ADMINISTRATOR);

        multiUser.addRole(multiRole4);
        inst2.addMemberWithRole(multiRole4);
        multiRole4.setRole(UserRole.STUDENT);

        user1.setEnabled(Boolean.TRUE);
        user2.setEnabled(Boolean.TRUE);
        multiUser.setEnabled(Boolean.TRUE);
        teacher.setEnabled(Boolean.TRUE);

        em.persist(user1);
        em.persist(user2);
        em.persist(multiUser);
        em.persist(teacher);
        em.persist(inst1);
        em.persist(inst2);
        em.persist(role1);
        em.persist(role2);

        //random data
        fillRandom();
        persistList(users);
        persistList(institutions);
        persistList(qualifications);
        persistList(subjects);
        persistList(DataLoader.roles);
        persistList(studentClasses);
        persistList(lessons);

//        Mark mark = new Mark();
//        User user = pick(students);
//        Lesson lesson = pick( user.getStudentsLessons() );
//        user.addMark(mark);
//        lesson.addMark(mark);
//        mark.setMark(4);
        LOGGER.info("random data generation finished");

    }

    //--------------------------------------------------------------------------
    
    public void fillRandom() {
        createQualifiactions(10);
        createSubjects();
        createInstitutions(nrOfInstitutions);
        createUsers(nrOfUsers);
        createRoles();
        createMultipleRoleUsers(nrOfUsers / 20);
        createStudentClasses();
        createLessonItems();
        createMarks();
        createNews();
        createMessages();
    }

    public <T> void persistList(List<T> list) {
        for (T t : list) {
            em.persist(t);
        }
    }

    public static String randomName(Gender gender) {
        String lastName = pick(lastNames), firstName;
        if (gender.equals(Gender.MALE)) {
            firstName = pick(maleFirstNames);
        } else {
            firstName = pick(femaleFirstNames);
        }
        return lastName + " " + firstName;
    }

    public static Day randomDay() {
        return Day.values()[(int) (Math.random() * 5)];
    }

    public static Date randomDate() {
        Date date = new Date();
        date.setTime((long) (Math.random() * 1576800000000L));
        return date;
    }

    public static String randomNumberString(int n) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < n; i++) {
            sb.append((int) (Math.random() * 10));
        }
        return sb.toString();
    }

    public static String randomPhoneNumberString(int n) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < n; i++) {
            sb.append((int) (Math.random() * 9 + 1));
        }
        return sb.toString();
    }

    public static String randomString(int n) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < n; i++) {
            sb.append((char) (Math.random() * 75 + 48));
        }
        return sb.toString();
    }

    public static String randomEmail(String name) {
        return name.toLowerCase().replace(' ', '_') + randomNumberString(4) + pick(emails);
    }

    public static Gender randomGender() {
        if (Math.random() < 0.5) {
            return Gender.MALE;
        } else {
            return Gender.FEMALE;
        }
    }

    public UserRole randomRole() {
        return UserRole.values()[(int) (Math.random() * UserRole.values().length)];
    }

    public static <T> T pick(List<T> list) {
        return list.get((int) (Math.random() * list.size()));
    }

    public static <T> List<T> pick(List<T> list, int n) {
        Set<T> s = new HashSet<>();
        while (s.size() < n) {
            s.add(pick(list));
        }
        List<T> result = new ArrayList<>();
        for (T t : s) {
            result.add(t);
        }
        return result;
    }

    //create
    public void createQualifiactions(int n) {
        for (int i = 0; i < n; i++) {
            Qualification q = new Qualification();
            q.setName(pick(qualifiactionNames) + "-" + pick(qualifiactionNames));
            //q.setUsers( ... );
            qualifications.add(q);
        }
    }

    public void createSubjects() {
        Subject subject = new Subject();
        subject.setName("anyanyelv");
        subjects.add(subject);
        //subject.setUsers( ... );
        subject = new Subject();
        subject.setName("magyar");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("számtan");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("matematika");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("természetismeret");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("fizika");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("kémia");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("testnevelés");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("ének-zene");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("biológia");
        subjects.add(subject);
        subject = new Subject();
        subject.setName("rajz");
        subjects.add(subject);
    }

    public void createUsers(int n) {
        for (int i = 0; i < n; i++) {
            User user = new User();
            user.setAddress(pick(cities));
            user.setBirthDate(randomDate());
            user.setBirthPlace(pick(cities));
            user.setGender(randomGender());
            user.setName(randomName(user.getGender()));
            user.setEmailAddress(randomEmail(replaceAccents(user.getName())));
            user.setMothersName(randomName(Gender.FEMALE));
            user.setPassword(passwordEncoder.encode("password"));
            user.setPersonalIdNumber(randomNumberString(5) + i);
            user.setPhoneNumber(randomPhoneNumberString(8));
            user.setUserName(replaceAccents(user.getName().toLowerCase().replace(' ', '_') + i));
            user.setEnabled(Boolean.TRUE);
            user.setUserName(replaceAccents(user.getName().toLowerCase().replace(' ', '_') + i));
            //user.setQualifications();
            //user.setParents(students);
            //user.setChildren();
//            user.setStudentClass();
//            user.setRoles();
//            user.setStudentsLessons();
//            user.setSubjects();
//            user.setTeachersLessons();
            users.add(user);
            if (Math.random() < 0.5) {
                adults.add(user);
            } else {
                students.add(user);
            }
        }
    }

    public void createInstitutions(int n) {
        for (int i = 0; i < n; i++) {
            Institution inst = new Institution();
            String city = pick(cities);
            inst.setAddress(city);
            inst.setLatitude(latitudes.get(city) * (1 + (Math.random()-0.5)/10000) );
            inst.setLongitude(longitudes.get(city) * (1 + (Math.random()-0.5)/10000) );
            inst.setContact(randomNumberString(8));
            inst.setName(inst.getAddress() + " " + pick(instituteNames) + " " + (i + 1));
            inst.setTaxNumber(randomNumberString(9));
            //inst.setMembers();
            institutions.add(inst);
        }
    }

    public void createRoles() {
        //local administrator, 2 admins per institution
        for (Institution inst : institutions) {
            List<User> locadmins = pick(adults, 2);
            for (User la : locadmins) {
                createSingleRole(la, inst, UserRole.LOCAL_ADMINISTRATOR);
            }
        }
        //students & parents
        for (User s : students) {
            Institution inst = pick(institutions);
            createSingleRole(s, inst, UserRole.STUDENT);
            User parent = pick(adults);
            parent.addChild(s);
            s.addParent(parent);
            createSingleRole(parent, inst, UserRole.PARENT);
            if (parent.getGender().equals(Gender.FEMALE)) {
                s.setMothersName(parent.getName());
            }
        }
        //teacher (only teacher role)
        int counter = 0;
        for (User user : adults) {
            if (user.getRoles().isEmpty()) {
                //Institution inst = pick(institutions);
                Institution inst = institutions.get(counter++ % nrOfInstitutions);
                createSingleRole(user, inst, UserRole.TEACHER);
                Qualification q = pick(qualifications);
                user.addQualification(q);
                q.addUser(user);
                Subject s = pick(subjects);
                user.addSubject(s);
                s.addUser(user);
            }
        }
    }

    public void createSingleRole(User user, Institution inst, UserRole userRole) {
        Role role = new Role();
        role.setInstitute(inst);
        role.setUser(user);
        role.setRole(userRole);
        if (!user.getRoles().contains(role)) {
            user.addRole(role);
            inst.addMemberWithRole(role);
            roles.add(role);
        }
    }

    public void createMultipleRoleUsers(int n) {
        for (int i = 0; i < n; i++) {
            User user = pick(adults);
            Institution inst = pick(institutions);
            UserRole randomrole;
            double r = Math.random();
            if (r < 0.2) {
                randomrole = UserRole.LOCAL_ADMINISTRATOR;
            } else if (r > 0.2 && r < 0.6) {
                randomrole = UserRole.STUDENT;
            } else {
                randomrole = UserRole.TEACHER;
            }
            Role role = new Role();
            role.setInstitute(inst);
            role.setUser(user);
            role.setRole(randomrole);
            if (!user.getRoles().contains(role)) {
                user.addRole(role);
                inst.addMemberWithRole(role);
                roles.add(role);
            }
        }
    }

    public void createStudentClasses() {
        for (Institution inst : institutions) {
            int n = inst.getMembers().size() / 20 + 1;
            List<StudentClass> localClasses = new ArrayList<>();
            List<User> localTeachers = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                StudentClass sc = new StudentClass();
                sc.setName((i + 1) + ".");
                sc.setStartYear((int) (Math.random() * 18 + 2000));
                sc.setBudget((int) (Math.random() * 10000));
                sc.setInstitution(inst);
                localClasses.add(sc);
                studentClasses.add(sc);
            }
            for (Role member : inst.getMembers()) {
                if (member.getRole().equals(UserRole.STUDENT)) {
                    StudentClass sc = pick(localClasses);
                    User student = member.getUser();
                    student.setStudentClass(sc);
                    sc.addStudent(student);
                } else if (member.getRole().equals(UserRole.TEACHER)) {
                    localTeachers.add(member.getUser());
                }
            }
            //lessons
            for (StudentClass lc : localClasses) {
                List<Subject> subjectList = pick(subjects, 5);
                for (Subject subject : subjectList) {
                    Lesson lesson = new Lesson();
                    lesson.setName(subject.getName() + " " + lc.getName());
                    lesson.setInstitution(inst);
                    lesson.setDay(randomDay());
                    lesson.setTime((int) (Math.random() * 3 + 8));
                    lesson.setLocation(inst.getName() + ", 1/1 terem");
                    lesson.setLessonsPerWeek((int) (Math.random() * 3 + 1));
                    lc.addLesson(lesson);
                    lessons.add(lesson);

                    User teacher = pick(localTeachers);
                    lesson.setTeacher(teacher);
                    teacher.addTeachersLesson(lesson);

                    lesson.setStudents(lc.getStudents());
                    for (User student : lc.getStudents()) {
                        student.addStudentsLesson(lesson);
                    }

                }
            }
        }
    }

    public void createLessonItems() {
        for (Lesson lesson : lessons) {
            for (int i = 0; i < lesson.getLessonsPerWeek(); i++) {
                LessonItem li = new LessonItem();
                li.setDay(randomDay());
                li.setHour((int) (Math.random() * 6 + 8));
                lesson.addLessonItem(li);
            }
        }
    }

    public void createMarks() {
        for (User student : students) {
            for (int i = 0; i < 8; i++) {
                Mark mark = new Mark();
                mark.setMark((int) (Math.random() * 5 + 1));
                student.addMark(mark);
                pick(student.getStudentsLessons()).addMark(mark);
            }
        }
    }

    public void createNews() {
        for (Institution inst : institutions) {
            News news = new News();
            news.setTitle("Üdvözlet");
            news.setText("A " + inst.getName() + " hírei.");
            inst.addNews(news);
        }
    }

    public void createMessages() {
        for (StudentClass sc : studentClasses) {
            Message message = new Message();
            message.setAuthor("Rendszer");
            message.setText("Sziasztok! A " + sc.getName() + " osztály üzenetei.");
            sc.addMessage(message);
        }
    }

    public static String replaceAccents(String s) {
        return s.replace('á', 'a')
                .replace('é', 'e')
                .replace('í', 'i')
                .replace('ó', 'o')
                .replace('ö', 'o')
                .replace('ő', 'o')
                .replace('ű', 'u')
                .replace('ú', 'u')
                .replace('ü', 'u');
    }

}
