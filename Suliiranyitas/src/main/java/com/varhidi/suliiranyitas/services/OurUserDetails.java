package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.User;
import com.varhidi.suliiranyitas.entities.UserRole;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Eszter
 */
public class OurUserDetails implements UserDetails {

    private User user;
    private Institution institution;
    private SimpleGrantedAuthority currentAuth;

    public OurUserDetails(User user) {
        this.user = user;
        currentAuth = new SimpleGrantedAuthority(user.getRoles().get(0).getRole().getAuthority());
        institution = user.getRoles().get(0).getInstitute();
    }

    public User getUser() {
        return user;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> auts = new ArrayList<>();
        if (user.isGlobalAdmin()) {
            auts.add(UserRole.GLOBAL_ADMINISTRATOR);
        }
        auts.add(currentAuth);
        return auts;
    }

    public SimpleGrantedAuthority getCurrentAuth() {
        return currentAuth;
    }

    public void setCurrentAuth(SimpleGrantedAuthority currentAuth) {
        this.currentAuth = currentAuth;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public String getUsername() {
        return user.getUserName();
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        //return user.getEnabled();
        return true;
    }

    public String getPassword() {
        return user.getPassword();
    }

    public boolean isTeacher() {
        return currentAuth.getAuthority().equals(UserRole.TEACHER.toString());
    }

    public boolean isStudent() {
        return currentAuth.getAuthority().equals(UserRole.STUDENT.toString());
    }

}
