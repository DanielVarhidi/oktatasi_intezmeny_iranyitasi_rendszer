/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author István
 */
public interface RoleRepository extends JpaRepository<Role, Long>{
    
}
