/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.entities.Role;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author István
 */
@Service
public class RoleService {
    
    @Autowired
    RoleRepository roleRepository;
    
    @PersistenceContext
    EntityManager em;
    public void save(Role role) {
        roleRepository.save(role);
    }
    
    @Transactional
    public void updateRoles(Role role){
        em.refresh(role);
    }
}
