package com.varhidi.suliiranyitas.services;

import com.varhidi.suliiranyitas.dtos.LessonItemDto;
import com.varhidi.suliiranyitas.dtos.LessonItemParentDto;
import com.varhidi.suliiranyitas.dtos.TimetableDto;
import com.varhidi.suliiranyitas.entities.Institution;
import com.varhidi.suliiranyitas.entities.LessonItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Kovács Dani
 */
@Service
//TODO make thees queries named query
public class TimetableService {
    
    @PersistenceContext
    EntityManager em;
    
    public List<LessonItemDto> getLessonItemsByStudent(String userName){
        List result = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemDto(sl.name, li.day, li.hour) FROM User user JOIN user.studentsLessons sl JOIN sl.lessonItems li WHERE user.userName = :usern")
            .setParameter("usern", userName)
            .getResultList();
        return result;
    }
    
    public List<TimetableDto> getTeacherTimetableByUsername(String userName){
        List lessonItemDtos = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemDto(tl.name, li.hour, li.day, tl.institution.name) FROM User user JOIN user.teachersLessons tl JOIN tl.lessonItems li WHERE user.userName = :usern")
            .setParameter("usern", userName)
            .getResultList();
        return lessonItemsToTimetables(lessonItemDtos);
    }
    
    public List<TimetableDto> getStudentTimetableByUsername(String userName){
        List lessonItemDtos = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemDto(sl.name, li.hour, li.day, li.lesson.institution.name, li.lesson.studentClass.name) FROM User user JOIN user.studentsLessons sl JOIN sl.lessonItems li WHERE user.userName = :usern")
            .setParameter("usern", userName)
            .getResultList();
        return lessonItemsToTimetables(lessonItemDtos);
    }
    
    public List<TimetableDto> getParentTimetableByUsername(String userName){
        List lessonItemDtos = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemDto(sl.name, li.hour, li.day, user.name, li.lesson.institution.name ) FROM User user JOIN user.parents parent JOIN user.studentsLessons sl JOIN sl.lessonItems li WHERE parent.userName = :usern")
            .setParameter("usern", userName)
            .getResultList();
        return lessonItemsToTimetables(lessonItemDtos);
    }
    
    private List<TimetableDto> lessonItemsToTimetables(List<LessonItemDto> list){
        Map<String, TimetableDto> timetableMap = new HashMap<>();
        for (Object lessonItemDto : list) {
            LessonItemDto lidto = (LessonItemDto) lessonItemDto;
            String key = lidto.getHeader1()+lidto.getHeader2();
            if (!timetableMap.containsKey(key)) {
                TimetableDto timetableDto = new TimetableDto();
                timetableDto.setTitle(lidto.getHeader1());
                timetableDto.setSubTitle(lidto.getHeader2());
                timetableMap.put(key, timetableDto);
            }
            timetableMap.get(key).getLessonItemDtos().add(lidto);
        }
        List<TimetableDto> result = new ArrayList<>();
        for (TimetableDto value : timetableMap.values()) {
            result.add(value);
        }
        return result;
    }
    
    public List<LessonItemDto> getLessonItemsByTeacher(String userName){
        List result = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemDto(tl.name, li.day, li.hour) FROM User user JOIN user.teachersLessons tl JOIN tl.lessonItems li WHERE user.userName = :usern")
            .setParameter("usern", userName)
            .getResultList();
        return result;
    }
    
    public List<LessonItemDto> getLessonItemsByInstitutionId(Long id){
        List result = em.createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemDto(l.name, li.id, li.day, li.hour, l.teacher.name, l.teacher.id, l.studentClass.name, l.studentClass.id) FROM Lesson l JOIN l.lessonItems li WHERE l.institution.id = :id")
            .setParameter("id", id)
            .getResultList();
        return result;
    }
    
    @Transactional
    public void updateTimetable(List<LessonItemDto> lessonItems){
        for (LessonItemDto lessonItem : lessonItems) {
            LessonItem li = em.find(LessonItem.class, lessonItem.getId());
            li.setDay(lessonItem.getDay());
            li.setHour(lessonItem.getHour()
            );
        }
    }
    
    public List<LessonItemParentDto> getLessonItemsByParentId(Long id){
        List result = em
                .createQuery("SELECT NEW com.varhidi.suliiranyitas.dtos.LessonItemParentDto(l.name, li.id, li.day, li.hour, child.name, l.institution.name) FROM User parent JOIN parent.children child JOIN child.studentsLessons l JOIN l.lessonItems li WHERE parent.id = :id")
                .setParameter("id", id)
                .getResultList();
        return result;
    }
    
    public Institution getInstitutionById(Long id){
        Institution inst = em.find(Institution.class, id);
        return inst;
    }
}
