package com.varhidi.suliiranyitas.dtos;

import com.varhidi.suliiranyitas.entities.UserRole;

/**
 *
 * @author Kovács Dani
 */
public class RoleDto {
    
    private String institutionName;
    private UserRole userRole;
    private Long id;

    public RoleDto() {
    }

    public RoleDto(Long id, String institutionName, UserRole userRole) {
        this.institutionName = institutionName;
        this.userRole = userRole;
        this.id = id;
    }

    
    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long Id) {
        this.id = Id;
    }
    
    public String getText(){
        return institutionName + " - " + userRole.name();
    }
    
}
