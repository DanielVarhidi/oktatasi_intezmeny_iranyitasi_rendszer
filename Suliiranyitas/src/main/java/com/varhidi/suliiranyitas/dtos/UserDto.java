package com.varhidi.suliiranyitas.dtos;

import com.varhidi.suliiranyitas.commons.DateConstants;
import com.varhidi.suliiranyitas.entities.Gender;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Kovács Dani
 */
public class UserDto {
    
    private Long id;
    
    @Column(unique = true, nullable = false)
    @Size(max = 50)
    private String userName;

    private String password;
    
    @Size(max = 50)
    private String name;

    @Size(max = 50)
    private String address;

    @Size(max = 50)
    private String emailAddress;

    @Size(max = 50)
    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+")
    private String phoneNumber;

    //@Column(unique = true, nullable = false)
    @Size(max = 50)
    private String personalIdNumber;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = DateConstants.DATE_PATTERN)
    private Date birthDate;

    @Size(max = 50)
    private String birthPlace;

    @Size(max = 50)
    private String mothersName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getMothersName() {
        return mothersName;
    }

    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
