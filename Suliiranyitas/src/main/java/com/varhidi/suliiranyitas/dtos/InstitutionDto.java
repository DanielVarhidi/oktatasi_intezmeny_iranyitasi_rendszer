package com.varhidi.suliiranyitas.dtos;
/**
 *
 * @author DaniKiss
 */
public class InstitutionDto {
    
    private long id;
    
    private String name;
    
    private String address;
    
    private String taxNumber;
    
    private String contact;
    
    private String localAdminEmailAdress;
    
    private double posLatitude;
    
    private double posLongitude;

    public String getLocalAdminEmailAdress() {
        return localAdminEmailAdress;
    }

    public void setLocalAdminEmailAdress(String localAdminEmailAdress) {
        this.localAdminEmailAdress = localAdminEmailAdress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPosLatitude() {
        return posLatitude;
    }

    public void setPosLatitude(double posLatitude) {
        this.posLatitude = posLatitude;
    }

    public double getPosLongitude() {
        return posLongitude;
    }

    public void setPosLongitude(double posLongitude) {
        this.posLongitude = posLongitude;
    }

    
}
