package com.varhidi.suliiranyitas.dtos;

import com.varhidi.suliiranyitas.entities.Day;
import com.varhidi.suliiranyitas.entities.StudentClass;
import com.varhidi.suliiranyitas.entities.User;

/**
 *
 * @author Kovács Dani
 */
public class LessonItemDto {
    
    private String name;
    private Long id;
    private Day day;
    private int hour;

    private String teacherName;
    private String studentClassName;
    
    private Long teacherId;
    private Long studentClassId;
    
    private User teacher;
    private StudentClass studentClass;
    
    private String header1 = "";
    private String header2 = "";
    
    public LessonItemDto(String name, Day day, int hour) {
        this.name = name;
        this.day = day;
        this.hour = hour;
    }

    public LessonItemDto() {
    }

    public LessonItemDto(String name, Day day, int hour, String teacherName, String studentClassName) {
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.teacherName = teacherName;
        this.studentClassName = studentClassName;
    }

    public LessonItemDto(String name, Day day, int hour, User teacher, StudentClass studentClass) {
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.teacher = teacher;
        this.studentClass = studentClass;
    }

    public LessonItemDto(String name, Long id, Day day, int hour, String teacherName, Long teacherId, String studentClassName, Long studentClassId) {
        this.name = name;
        this.id = id;
        this.day = day;
        this.hour = hour;
        this.teacherName = teacherName;
        this.studentClassName = studentClassName;
        this.teacherId = teacherId;
        this.studentClassId = studentClassId;
    }

    public LessonItemDto(Long id, Day day, int hour, Long teacherId, Long studentClassId) {
        this.id = id;
        this.day = day;
        this.hour = hour;
        this.teacherId = teacherId;
        this.studentClassId = studentClassId;
    }
    
    public LessonItemDto(String name, int hour, Day day, String header1, String header2){
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.header1 = header1;
        this.header2 = header2;
    }
    
    public LessonItemDto(String name, int hour, Day day, String header1){
        this.name = name;
        this.day = day;
        this.hour = hour;
        this.header1 = header1;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day dat) {
        this.day = dat;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStudentClassName() {
        return studentClassName;
    }

    public void setStudentClassName(String studentClassName) {
        this.studentClassName = studentClassName;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public StudentClass getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(StudentClass studentClass) {
        this.studentClass = studentClass;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentClassId() {
        return studentClassId;
    }

    public void setStudentClassId(Long studentClassId) {
        this.studentClassId = studentClassId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeader1() {
        return header1;
    }

    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }

   
}
