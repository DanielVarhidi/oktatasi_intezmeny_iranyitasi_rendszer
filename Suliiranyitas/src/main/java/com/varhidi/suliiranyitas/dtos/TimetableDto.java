package com.varhidi.suliiranyitas.dtos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kovács Dani
 */
public class TimetableDto {
    
    private String title;
    private String subTitle;
    private List<LessonItemDto> lessonItemDtos = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public List<LessonItemDto> getLessonItemDtos() {
        return lessonItemDtos;
    }

    public void setLessonItemDtos(List<LessonItemDto> lessonItemDtos) {
        this.lessonItemDtos = lessonItemDtos;
    }

    public void addLessonItemDto(LessonItemDto dto){
        lessonItemDtos.add(dto);
    }
    
    public void removeLessonItemDto(LessonItemDto dto){
        lessonItemDtos.remove(dto);
    }
}
