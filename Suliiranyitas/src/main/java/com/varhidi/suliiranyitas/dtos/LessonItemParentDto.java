package com.varhidi.suliiranyitas.dtos;

import com.varhidi.suliiranyitas.entities.Day;

/**
 *
 * @author Kovács Dani
 */
public class LessonItemParentDto {
    
    private String name;
    private Long id;
    private Day day;
    private int hour;
    
    private String studentName;
    private String institutionName;

    public LessonItemParentDto() {
    }

    public LessonItemParentDto(String name, Long id, Day day, int hour, String studentName, String insitutionName) {
        this.name = name;
        this.id = id;
        this.day = day;
        this.hour = hour;
        this.studentName = studentName;
        this.institutionName = insitutionName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String insitutionName) {
        this.institutionName = insitutionName;
    }
    
    
}
